<?php

use Illuminate\Database\Seeder;
use App\Models\Website;

class WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
		Website::insert([
		    [
		        'title'=>'Coder 1', 
		        'description'=>'4096',
		        'created_at'=>date('Y-m-d H:i:s')
		    ],
		    [
		         'title'=>'Coder 2', 
		         'description'=>'2048',
		         'created_at'=>date('Y-m-d H:i:s')
		    ] 
		]);
    }
}
