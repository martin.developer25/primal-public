<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driver', 64)->default('smtp')->comment('smtp, mailgun, mailtrap');
            $table->string('host', 128)->default('mailtrap.io');
            $table->string('port', 6)->default('2525');
            $table->string('username', 64)->nullable();
            $table->string('password', 64)->nullable();
            $table->string('encryption', 20)->nullable()->default('tls')->comment('tls');
            $table->string('sendmail', 64)->default('usr/sbin/sendmail -bs');
            $table->string('pretend', 10)->nullable()->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_setting');
    }
}
