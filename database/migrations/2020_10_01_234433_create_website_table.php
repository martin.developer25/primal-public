<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('menu', 50)->nullable();
            $table->enum('type', ['section', 'slider', 'page'])->nullable()->default('section');
            $table->string('title', 128)->nullable();
            $table->string('sub_title')->nullable();
            $table->text('description')->nullable();
            $table->string('image', 128)->nullable();
            $table->string('url')->nullable();
            $table->tinyInteger('position')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(1);
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website');
    }
}
