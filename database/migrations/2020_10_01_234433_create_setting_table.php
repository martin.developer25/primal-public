<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 100);
            $table->string('description')->nullable();
            $table->string('logo', 50)->nullable();
            $table->string('favicon', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone', 16)->nullable();
            $table->string('address')->nullable();
            $table->string('copyright_text')->nullable();
            $table->string('direction', 10)->nullable();
            $table->string('language', 10)->nullable();
            $table->string('timezone', 32)->default('Asia/Dhaka');
            $table->boolean('website')->default(0);
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('google_map', 512)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
