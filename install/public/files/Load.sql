-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Dumping structure for table db_queue_pda.counter
DROP TABLE IF EXISTS `counter`;
CREATE TABLE IF NOT EXISTS `counter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.counter: ~16 rows (approximately)
DELETE FROM `counter`;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
INSERT INTO `counter` (`id`, `name`, `description`, `created_at`, `updated_at`, `status`) VALUES
  (1, '1', NULL, '2020-04-15', '2020-07-11', 1),
  (2, '2', NULL, '2020-04-15', '2018-07-27', 1),
  (3, '3', NULL, '2020-04-15', '2018-07-27', 1),
  (4, '4', NULL, '2020-04-15', '2018-07-27', 1),
  (5, '5', NULL, '2020-04-15', '2018-07-27', 1),
  (6, '6', NULL, '2020-04-15', '2018-07-27', 1),
  (7, '7', NULL, '2020-04-15', '2018-07-27', 1),
  (8, '8', NULL, '2020-04-15', '2018-07-27', 1),
  (9, '9', NULL, '2020-04-15', '2018-07-27', 1),
  (10, '10', NULL, '2020-04-15', '2018-07-27', 1),
  (11, '11', NULL, '2020-04-15', '2018-07-27', 1),
  (12, '12', NULL, '2020-04-15', '2018-07-27', 1),
  (13, '13', NULL, '2020-04-15', '2018-07-27', 1),
  (14, '14', NULL, '2020-04-15', '2018-07-27', 1),
  (15, '15', NULL, '2020-04-15', '2018-07-27', 1),
  (16, '16', NULL, '2020-04-15', '2018-07-27', 1);
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.department
DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `key` varchar(1) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.department: ~7 rows (approximately)
DELETE FROM `department`;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`id`, `name`, `description`, `key`, `created_at`, `updated_at`, `status`, `img`) VALUES
  (1, 'Charcuteria 1', 'Apple department', 'a', '2016-10-31 10:34:19', '2020-07-18 17:08:00', 1, 'line-sausages.png'),
  (2, 'Carniceria 2', 'Banana Department', 'b', '2016-11-09 07:18:01', '2020-07-18 17:08:17', 1, 'line-meat.png');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.display
DROP TABLE IF EXISTS `display`;
CREATE TABLE IF NOT EXISTS `display` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text,
  `direction` varchar(10) DEFAULT 'left',
  `color` varchar(10) DEFAULT '#ffffff',
  `background_color` varchar(10) NOT NULL DEFAULT '#cdcdcd',
  `border_color` varchar(10) NOT NULL DEFAULT '#ffffff',
  `time_format` varchar(20) DEFAULT 'h:i:s A',
  `date_format` varchar(50) DEFAULT 'd M, Y',
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-single, 2/3-counter,4-department,5-hospital',
  `keyboard_mode` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0-inactive,1-active',
  `sms_alert` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0-inactive, 1-active ',
  `show_note` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-inactive, 1-active ',
  `show_officer` tinyint(1) NOT NULL DEFAULT '1',
  `show_department` tinyint(1) NOT NULL DEFAULT '1',
  `alert_position` int(2) NOT NULL DEFAULT '3',
  `language` varchar(20) DEFAULT 'English',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.display: ~0 rows (approximately)
DELETE FROM `display`;
/*!40000 ALTER TABLE `display` DISABLE KEYS */;
INSERT INTO `display` (`id`, `message`, `direction`, `color`, `background_color`, `border_color`, `time_format`, `date_format`, `updated_at`, `display`, `keyboard_mode`, `sms_alert`, `show_note`, `show_officer`, `show_department`, `alert_position`, `language`) VALUES
  (1, 'Token - Queue Management System', 'left', '#ff0404', '#000000', '#3c8dbc', 'H:i:s', 'd M, Y', '2020-07-18 13:21:04', 5, 0, 0, 0, 1, 1, 2, 'English');
/*!40000 ALTER TABLE `display` ENABLE KEYS */; 

-- Dumping structure for table db_queue_dev.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Dumping structure for table db_queue_dev.mail_history
DROP TABLE IF EXISTS `mail_history`;
CREATE TABLE `mail_history` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
  `subject` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
  `message` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
  `schedule_at` TIMESTAMP NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` INT(10) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0-pending, 1-sent, 2-quick-send',
  PRIMARY KEY (`id`) USING BTREE
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;



-- Dumping data for table db_queue_dev.mail_history: ~27 rows (approximately)
DELETE FROM `mail_history`;
/*!40000 ALTER TABLE `mail_history` DISABLE KEYS */;
INSERT INTO `mail_history` (`id`, `email`, `subject`, `message`, `schedule_at`, `created_at`, `updated_at`, `created_by`, `status`) VALUES
  (2, 'admin@example.com', 'AFdf', 'Test message', '2020-09-21 16:54:58', '2020-09-20 16:54:45', '2020-09-22 19:22:51', 1, 1),
  (3, 'admin1@example.com', 'FES', 'Test SSS', '2020-09-21 16:54:58', '2020-09-11 16:54:45', '2020-09-22 13:32:36', 1, 1),
  (4, 'admin2@example.com', 'BVS', 'Test message XX', '2020-09-21 16:54:58', '2020-09-13 16:54:45', '2020-09-22 13:32:41', 1, 1),
  (5, 'admin3@example.com', 'Test', 'Test FREE', '2020-09-21 16:54:58', '2020-09-21 16:54:45', NULL, 1, 1),
  (6, 'admin@codekernel.nt', 'Test', 'tesrhkjh kjg', NULL, '2020-09-21 22:43:00', NULL, 1, 1),
  (7, 'admin@codekernel.nt', 'Test', 'tesrhkjh kjg', NULL, '2020-09-21 22:44:11', '2020-09-22 13:38:18', 1, 1),
  (8, 'admin@codekernel.nt', 'Test', 'tesrhkjh kjg', NULL, '2020-09-21 23:05:19', '2020-09-22 13:38:23', 1, 1),
  (9, 'admin@codekernel.nt', 'Test mail', 'First, we’ll add an overall structure for our email, starting with a <body> tag. We\'ll set the margin and padding on the body tag to zero to avoid any unexpected space.\r\n\r\nWe’ll also add a table with a width of 100%. This acts as a true body tag for our email, because styling of the body tag isn’t fully supported. If you wanted to add a background color to the ‘body’ of your email, you’d need to apply it to this big table instead.\r\n\r\nSet your cellpadding and cellspacing to zero to avoid any unexpected space in the table.\r\n\r\nNote: We’re going to leave border="1" on all of our tables, so that we can see the skeleton of our layout as we go. We’ll remove them at the end with a simple Find & Replace.', NULL, '2020-09-21 23:12:33', '2020-09-22 13:38:28', 1, 1),
  (10, 'admin@codekernel.nt', 'Test mail', 'Test', NULL, '2020-09-22 00:07:57', '2020-09-22 13:38:32', 1, 1),
  (11, 'admin@codekernel.nt', 'Test mail', '_message', NULL, '2020-09-22 00:51:54', '2020-09-22 13:38:37', 1, 1),
  (12, 'jane@doe.com', 'Object of class Illuminate\\Mail\\Message could not be converted to string', 'i don\'t know why its a bug of laravel but $message is blacklisted for mail on view\r\nwhen i tried {{ $message }} getting error and not sending the message\r\nwhen i change $message to any name like {{ $content }} its working fine yeah its working fine now thanks for contribute all', NULL, '2020-09-22 00:56:49', '2020-09-22 13:38:43', 1, 1),
  (13, 'admin@codekernel.nt', 'Test mail', '14\r\n\r\nIn order to make Laravel 5/6 full queuing you need make below steps:\r\n\r\nphp artisan queue:table (for jobs)\r\nphp artisan queue:failed-table (for failed jobs)\r\nphp artisan migrate\r\nSet in .env QUEUE_DRIVER=database\r\nFire: php artisan config:cache\r\nFire queuing: php artisan queue:work database --tries=1 (after all uncompleted tries it will be registered in failed jobs table)\r\nSince sending email messages can drastically lengthen the response time of your application, many developers choose to queue email messages for background sending. Laravel makes this easy using its built-in unified queue API. To queue a mail message, use the queue method on the Mail facade after specifying the message\'s recipients:', NULL, '2020-09-22 11:54:05', '2020-09-22 13:38:47', 1, 1),
  (14, 'admin@codekernel.nt', 'Test mail', 'Atif Aslam mashup 2018 || Love Song || Mad 4 Beats', NULL, '2020-09-22 19:00:52', NULL, 1, 1);
/*!40000 ALTER TABLE `mail_history` ENABLE KEYS */;

-- Dumping structure for table db_queue_dev.mail_setting
DROP TABLE IF EXISTS `mail_setting`;
CREATE TABLE IF NOT EXISTS `mail_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'smtp' COMMENT 'smtp, mailgun, mailtrap',
  `host` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mailtrap.io',
  `port` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2525',
  `username` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encryption` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'tls' COMMENT 'tls',
  `sendmail` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'usr/sbin/sendmail -bs',
  `pretend` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'false',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_queue_dev.mail_setting: ~0 rows (approximately)
DELETE FROM `mail_setting`;
/*!40000 ALTER TABLE `mail_setting` DISABLE KEYS */;
INSERT INTO `mail_setting` (`id`, `driver`, `host`, `port`, `username`, `password`, `encryption`, `sendmail`, `pretend`) VALUES
  (1, 'smtp', 'smtp.mailtrap.io', '2525', 'username', 'password', 'tls', 'usr/sbin/sendmail -bs', '0');
/*!40000 ALTER TABLE `mail_setting` ENABLE KEYS */;


-- Dumping structure for table db_queue_pda.message
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `attachment` varchar(128) DEFAULT NULL, 
  `datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sender_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unseen, 1=seen, 2=delete',
  `receiver_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unseen, 1=seen, 2=delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
 
-- Dumping data for table db_queue_pda.message: ~8 rows (approximately)
DELETE FROM `message`;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`id`, `sender_id`, `receiver_id`, `subject`, `message`, `attachment`, `datetime`, `sender_status`, `receiver_status`) VALUES
  (1, 1, 5, 'Tedsf', 'gffg', NULL, '2019-07-29 05:54:00', 0, 1),
  (2, 1, 2, 'adf', 'ghdf', NULL, '2018-07-29 05:54:00', 0, 0),
  (3, 1, 3, 'hg', 'efff', NULL, '2018-07-29 05:54:00', 1, 1),
  (4, 1, 4, '3fsa', 'dasf', NULL, '2018-07-29 05:54:00', 0, 1),
  (5, 5, 1, '33', 'ewrf', NULL, '2018-07-29 05:54:00', 0, 1),
  (6, 2, 1, 'dc', 'afsc', NULL, '2018-07-29 05:54:00', 0, 1),
  (7, 3, 1, 'asdf', 'xcvs', NULL, '2018-07-29 05:54:00', 0, 1),
  (8, 4, 1, 'sx', 'exf', NULL, '2018-07-29 05:54:00', 0, 1),
  (9, 1, 6, 'AAA1', 'TAFD', NULL, '2020-07-09 22:25:00', 0, 0),
  (10, 2, 7, 'AAA1', 'TSFD', NULL, '2020-07-09 10:32:46', 0, 0),
  (11, 1, 5, 'dd', 'TEST', 'public/assets/attachments/69865.jpg', '2020-07-11 10:38:44', 0, 1),
  (12, 1, 7, 'ef', 'Test', 'public/assets/attachments/43195.jpg', '2020-07-13 00:11:47', 0, 0),
  (13, 1, 8, 'Test Subject', 'cy: No \'Access-Control-Allow-Origin\' head', 'public/assets/attachments/33884.jpg', '2020-07-13 15:23:44', 0, 1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

-- Dumping structure for table db_queue_dev.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

 
-- Dumping structure for table db_queue_dev.setting
DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `favicon` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `copyright_text` varchar(255) DEFAULT NULL,
  `direction` varchar(10) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `timezone` varchar(32) NOT NULL DEFAULT 'Asia/Dhaka',
  `website` tinyint(1) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `google_map` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_dev.setting: ~1 rows (approximately)
DELETE FROM `setting`;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `title`, `description`, `logo`, `favicon`, `email`, `phone`, `address`, `copyright_text`, `direction`, `language`, `timezone`, `website`, `meta_title`, `meta_keyword`, `meta_description`, `google_map`) VALUES
  (1, 'Token - Queue Management System', 'Queue', 'public/assets/img/icons/logo.jpg', 'public/assets/img/icons/favicon.jpg', 'admin@example.com', '+325 252 222', 'Demo street, NY-10000', 'copyright@2020', NULL, 'en', 'Asia/Dhaka', 1, 'Token - Queue Management System', 'Token, Queue, CodeKernel, Serial, Dispanser', 'Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.', 'https://maps.google.com/maps?width=1000&height=440&hl=en&q=New%20York%2C%20USA+(CodeKernel)&ie=UTF8&t=&z=17&iwloc=B&output=embed');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.sms_history
DROP TABLE IF EXISTS `sms_history`;
CREATE TABLE IF NOT EXISTS `sms_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(20) DEFAULT NULL,
  `to` varchar(20) DEFAULT NULL,
  `message` varchar(512) DEFAULT NULL,
  `response` varchar(512) DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.sms_history: ~31 rows (approximately)
DELETE FROM `sms_history`;
/*!40000 ALTER TABLE `sms_history` DISABLE KEYS */;
INSERT INTO `sms_history` (`id`, `from`, `to`, `message`, `response`, `created_at`) VALUES
  (3, 'Queue Management Sys', '8801821742285', 'Test', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"status\\": \\"4\\",\\n        \\"error-text\\": \\"Bad Credentials\\"\\n    }]\\n}","message":"Test"}', '2020-04-28 16:03:09'),
  (4, 'Queue Management Sys', '8801821742285', 'Test', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"status\\": \\"3\\",\\n        \\"error-text\\": \\"Invalid from param\\"\\n    }]\\n}","message":"Test"}', '2020-04-28 22:05:19'),
  (5, 'Queue Management Sys', '8801821742285', 'TEST B', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"8801821742285\\",\\n        \\"status\\": \\"29\\",\\n        \\"error-text\\": \\"Non White-listed Destination - rejected\\"\\n    }]\\n}","message":"TEST B"}', '2020-04-28 23:25:59'),
  (6, 'Queue Management Sys', '3367019711', 'TEST B', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"3367019711\\",\\n        \\"status\\": \\"29\\",\\n        \\"error-text\\": \\"Non White-listed Destination - rejected\\"\\n    }]\\n}","message":"TEST B"}', '2020-04-28 23:27:20'),
  (7, 'Queue Management Sys', '0123456789', 'Token No: A106 \\r\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\r\\n Your waiting no is 2. \\r\\n 2020-05-14 23:44:49.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A106 \\\\r\\\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\\\r\\\\n Your waiting no is 2. \\\\r\\\\n 2020-05-14 23:44:49."}', '2020-05-14 23:59:49'),
  (8, 'Queue Management Sys', '0123456789', 'Token No: A204 \\r\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:52:00.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A204 \\\\r\\\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:52:00."}', '2020-05-14 23:59:50'),
  (9, 'Queue Management Sys', '0123456789', 'Token No: A304 \\r\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:52:06.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A304 \\\\r\\\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:52:06."}', '2020-05-14 23:59:51'),
  (10, 'Queue Management Sys', '0123456789', 'Token No: A107 \\r\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\r\\n Your waiting no is 1. \\r\\n 2020-05-14 23:45:24.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A107 \\\\r\\\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-14 23:45:24."}', '2020-05-15 00:00:06'),
  (11, 'Queue Management Sys', '0123456789', 'Token No: A203 \\r\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:51:45.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A203 \\\\r\\\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:51:45."}', '2020-05-15 00:00:07'),
  (12, 'Queue Management Sys', '0123456789', 'Token No: A303 \\r\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:51:49.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A303 \\\\r\\\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:51:49."}', '2020-05-15 00:00:07'),
  (13, 'Queue Management Sys', '0123456789', 'Token No: A202 \\r\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:51:29.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A202 \\\\r\\\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:51:29."}', '2020-05-15 00:00:11'),
  (14, 'Queue Management Sys', '0123456789', 'Token No: A302 \\r\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:51:39.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A302 \\\\r\\\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:51:39."}', '2020-05-15 00:00:12'),
  (15, 'Queue Management Sys', '0123456789', 'Token No: A201 \\r\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:51:07.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A201 \\\\r\\\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:51:07."}', '2020-05-15 00:00:16'),
  (16, 'Queue Management Sys', '0123456789', 'Token No: A301 \\r\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\r\\n Your waiting no is 3. \\r\\n 2020-05-14 23:51:23.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A301 \\\\r\\\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-14 23:51:23."}', '2020-05-15 00:00:16'),
  (17, 'Queue Management Sys', '0123456789', 'Token No: A101 \\r\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\r\\n Your waiting no is 2. \\r\\n 2020-05-15 00:20:34.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A101 \\\\r\\\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\\\r\\\\n Your waiting no is 2. \\\\r\\\\n 2020-05-15 00:20:34."}', '2020-05-15 00:48:46'),
  (18, 'Queue Management Sys', '0123456789', 'Token No: O502 \\r\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\r\\n Your waiting no is 3. \\r\\n 2020-05-15 00:20:39.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: O502 \\\\r\\\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\\\r\\\\n Your waiting no is 3. \\\\r\\\\n 2020-05-15 00:20:39."}', '2020-05-15 00:48:47'),
  (19, 'Queue Management Sys', '0123456789', 'Token No: A105 \\r\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 00:54:46.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A105 \\\\r\\\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 00:54:46."}', '2020-05-15 01:18:43'),
  (20, 'Queue Management Sys', '0123456789', 'Token No: O504 \\r\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 00:54:52.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: O504 \\\\r\\\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 00:54:52."}', '2020-05-15 01:18:44'),
  (21, 'Queue Management Sys', '0123456789', 'Token No: A106 \\r\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 01:09:26.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A106 \\\\r\\\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 01:09:26."}', '2020-05-15 01:18:54'),
  (22, 'Queue Management Sys', '0123456789', 'Token No: O505 \\r\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 00:54:57.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: O505 \\\\r\\\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 00:54:57."}', '2020-05-15 01:18:55'),
  (23, 'Queue Management Sys', '0123456789', 'Token No: A104 \\r\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 00:54:14.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A104 \\\\r\\\\n Department: Apple, Counter: 1 and Officer: Wane Willian. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 00:54:14."}', '2020-05-15 02:20:50'),
  (24, 'Queue Management Sys', '0123456789', 'Token No: A205 \\r\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 01:06:30.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A205 \\\\r\\\\n Department: Apple, Counter: 2 and Officer: Jane Doe. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 01:06:30."}', '2020-05-15 02:20:51'),
  (25, 'Queue Management Sys', '0123456789', 'Token No: A304 \\r\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 00:54:35.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: A304 \\\\r\\\\n Department: Apple, Counter: 3 and Officer: Annie Smith. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 00:54:35."}', '2020-05-15 02:20:52'),
  (26, 'Queue Management Sys', '0123456789', 'Token No: O502 \\r\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\r\\n Your waiting no is 1. \\r\\n 2020-05-15 00:54:26.', '{"status":true,"request_url":"https:\\/\\/rest.nexmo.com\\/sms\\/json?","success":"{\\n    \\"message-count\\": \\"1\\",\\n    \\"messages\\": [{\\n        \\"to\\": \\"0123456789\\",\\n        \\"status\\": \\"6\\",\\n        \\"error-text\\": \\"Unroutable message - rejected\\"\\n    }]\\n}","message":"Token No: O502 \\\\r\\\\n Department: Orange, Counter: 5 and Officer: Alex Smith. \\\\r\\\\n Your waiting no is 1. \\\\r\\\\n 2020-05-15 00:54:26."}', '2020-05-15 02:20:52'),
  (32, '01919742285', '8801821742285', 'test', '{"status":true,"request_url":"https:\\/\\/platform.clickatell.com\\/messages\\/http\\/send?","success":"{\\"messages\\":[],\\"errorCode\\":607,\\"error\\":\\"Invalid FROM number.\\",\\"errorDescription\\":\\"User specified FROM number, but integration isn\'t two-way.\\"}","message":"test"}', '2020-05-17 14:12:10'),
  (33, '8801919742285', '8801821742285', 'test', '{"status":true,"request_url":"https:\\/\\/platform.clickatell.com\\/messages\\/http\\/send?","success":"{\\"messages\\":[],\\"errorCode\\":607,\\"error\\":\\"Invalid FROM number.\\",\\"errorDescription\\":\\"User specified FROM number, but integration isn\'t two-way.\\"}","message":"test"}', '2020-05-17 14:18:48'),
  (34, '8801919742285', '8801821742285', 'TEST', '{"status":true,"request_url":"https:\\/\\/platform.clickatell.com\\/messages\\/http\\/send?","success":"{\\"messages\\":[{\\"apiMessageId\\":\\"d737eadad6f9476ca91924a8cf31a661\\",\\"accepted\\":true,\\"to\\":\\"8801821742285\\",\\"errorCode\\":null,\\"error\\":null,\\"errorDescription\\":null}]}","message":"TEST"}', '2020-05-17 14:24:49'),
  (35, '7082747358', '8801821742285', 'TEST', '{"status":true,"request_url":"https:\\/\\/platform.clickatell.com\\/messages\\/http\\/send?","success":"{\\"messages\\":[],\\"errorCode\\":607,\\"error\\":\\"Invalid FROM number.\\",\\"errorDescription\\":\\"User specified FROM number, but integration isn\'t two-way.\\"}","message":"TEST"}', '2020-05-17 14:30:49'),
  (36, '17082747358', '8801821742285', 'TEST', '{"status":true,"request_url":"https:\\/\\/platform.clickatell.com\\/messages\\/http\\/send?","success":"{\\"messages\\":[{\\"apiMessageId\\":\\"c5d7a69898ef43348e9b3cd7ce7a5096\\",\\"accepted\\":true,\\"to\\":\\"8801821742285\\",\\"errorCode\\":null,\\"error\\":null,\\"errorDescription\\":null}]}","message":"TEST"}', '2020-05-17 14:45:38');
/*!40000 ALTER TABLE `sms_history` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.sms_setting
DROP TABLE IF EXISTS `sms_setting`;
CREATE TABLE IF NOT EXISTS `sms_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(20) NOT NULL DEFAULT 'nexmo',
  `api_key` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `from` varchar(50) DEFAULT NULL,
  `sms_template` text,
  `recall_sms_template` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.sms_setting: ~0 rows (approximately)
DELETE FROM `sms_setting`;
/*!40000 ALTER TABLE `sms_setting` DISABLE KEYS */;
INSERT INTO `sms_setting` (`id`, `provider`, `api_key`, `username`, `password`, `from`, `sms_template`, `recall_sms_template`) VALUES
  (1, 'clickatell', '-K1xA==', 'codekernel', '05kOeOvm', '11222747358', 'Token No: [TOKEN] \\r\\n Department: [DEPARTMENT], Counter: [COUNTER] and Officer: [OFFICER]. \\r\\n Your waiting no is [WAIT]. \\r\\n [DATE].', 'Please contact urgently. Token No: [TOKEN] \\r\\n Department: [DEPARTMENT], Counter: [COUNTER] and Officer: [OFFICER]. \\r\\n [DATE].');
/*!40000 ALTER TABLE `sms_setting` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.token
DROP TABLE IF EXISTS `token`;
CREATE TABLE IF NOT EXISTS `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_no` varchar(10) DEFAULT NULL,
  `client_mobile` varchar(20) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `counter_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `note` varchar(512) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_vip` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-pending, 1-complete, 2-stop',
  `sms_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-pending, 1-sent, 2-quick-send',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.token: ~14 rows (approximately)
DELETE FROM `token`;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;

INSERT INTO `token` (`id`, `token_no`, `client_mobile`, `department_id`, `counter_id`, `user_id`, `note`, `created_by`, `created_at`, `updated_at`, `is_vip`, `status`, `sms_status`) VALUES  
(1, 'A101', '0123456789', 1, 1, 2, NULL, 2, DATE_ADD(NOW(), INTERVAL -7 DAY), NOW(), NULL, 1, 1),
(2, 'A201', '0123456789', 1, 2, 2, NULL, 2, DATE_ADD(NOW(), INTERVAL -7 DAY), NOW(), NULL, 1, 1),
(3, 'A202', '0123456789', 1, 2, 4, NULL, NULL, DATE_ADD(NOW(), INTERVAL -7 DAY), NOW(), NULL, 1, 1),
(4, 'A301', '0123456789', 1, 3, 4, NULL, 2, DATE_ADD(NOW(), INTERVAL -6 DAY), NOW(), NULL, 1, 1),
(5, 'VA302', '0123456789', 1, 3, 4, NULL, 2, DATE_ADD(NOW(), INTERVAL -5 DAY), NULL, 1, 0, 0),
(6, 'VC501', '0123456789', 3, 5, 1, NULL, NULL, DATE_ADD(NOW(), INTERVAL -5 DAY), NULL, 1, 0, 0),
(7, 'VC502', '0123456789', 3, 5, 1, NULL, NULL, DATE_ADD(NOW(), INTERVAL -4 DAY), NULL, 1, 0, 0),
(8, 'B401', '0123456789', 2, 4, 5, NULL, 2, DATE_ADD(NOW(), INTERVAL -4 DAY), NULL, NULL, 0, 0),
(9, 'O601', '0123456789', 4, 6, 4, NULL, 1, DATE_ADD(NOW(), INTERVAL -4 DAY), NULL, NULL, 0, 0),
(10, 'O602', '0123456789', 4, 6, 5, NULL, 1, DATE_ADD(NOW(), INTERVAL -4 DAY), NOW(), NULL, 1, 0),
(11, 'A101', '0123456789', 1, 1, 2, NULL, 1, DATE_ADD(NOW(), INTERVAL -4 DAY), NULL, NULL, 0, 0),
(12, 'A201', '0123456789', 1, 2, 2, NULL, 1, DATE_ADD(NOW(), INTERVAL -4 DAY), NULL, NULL, 0, 0),
(13, 'VA202', '0123456789', 1, 2, 1, NULL, 1, DATE_ADD(NOW(), INTERVAL -3 DAY), NOW(), 1, 1, 0),
(14, 'A301', '0123456789', 1, 3, 4, NULL, 1, DATE_ADD(NOW(), INTERVAL -3 DAY), NULL, NULL, 0, 0),
(15, 'A302', '0123456789', 1, 3, 4, NULL, NULL, DATE_ADD(NOW(), INTERVAL -2 DAY), NULL, NULL, 0, 0),
(16, 'C501', '0123456789', 3, 5, 1, NULL, NULL, DATE_ADD(NOW(), INTERVAL -2 DAY), NOW(), NULL, 1, 0),
(17, 'C502', '0123456789', 3, 5, 1, NULL, 3, DATE_ADD(NOW(), INTERVAL -2 DAY), NULL, NULL, 0, 0),
(18, 'B401', '0123456789', 2, 4, 5, NULL, 4, DATE_ADD(NOW(), INTERVAL -2 DAY), NULL, NULL, 0, 0),
(19, 'O601', '0123456789', 4, 6, 8, NULL, 5, DATE_ADD(NOW(), INTERVAL -2 DAY), NOW(), NULL, 1, 0),
(20, 'O602', '0123456789', 4, 6, 2, NULL, NULL, DATE_ADD(NOW(), INTERVAL -1 DAY), NULL, NULL, 0, 0),
(22, 'A101', '0123456789', 1, 1, 2, NULL, 3, NOW(), NULL, NULL, 2, 0), 
(23, 'VA201', '0123456789', 1, 2, 4, NULL, 2, NOW(), NULL, 1, 1, 0),
(24, 'A202', '0123456789', 1, 2, 2, NULL, 1, NOW(), NULL, NULL, 0, 0),
(25, 'A301', '0123456789', 1, 3, 4, NULL, 2, NOW(), NULL, NULL, 1, 0),
(26, 'A302', '0123456789', 1, 3, 4, NULL, 4, NOW(), NULL, NULL, 0, 0),
(27, 'VC501', '0123456789', 3, 5, 1, NULL, 5, NOW(), NULL, 1, 1, 0),
(28, 'C502', '0123456789', 3, 5, 1, NULL, 3, NOW(), NULL, NULL, 2, 0),
(29, 'B401', '0123456789', 2, 4, 5, NULL, 6, NOW(), NULL, NULL, 0, 0),
(30, 'O601', '0123456789', 4, 6, 6, NULL, 7, NOW(), NULL, NULL, 2, 0),
(31, 'O602', '0123456789', 4, 6, 7, NULL, 8, NOW(), NULL, NULL, 2, 0);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.token_setting
DROP TABLE IF EXISTS `token_setting`;
CREATE TABLE IF NOT EXISTS `token_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `counter_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.token_setting: ~8 rows (approximately)
DELETE FROM `token_setting`;
/*!40000 ALTER TABLE `token_setting` DISABLE KEYS */;
INSERT INTO `token_setting` (`id`, `department_id`, `counter_id`, `user_id`, `created_at`, `updated_at`, `status`) VALUES
  (7, 1, 1, 2, '2020-05-14 23:43:49', NULL, 1),
  (8, 2, 2, 4, '2020-05-14 23:50:42', NULL, 1);
/*!40000 ALTER TABLE `token_setting` ENABLE KEYS */;

-- Dumping structure for table db_queue_pda.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(25) DEFAULT NULL,
  `lastname` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `about` varchar(512) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `user_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=officer, 2=staff, 3=client, 5=admin',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active,2=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_pda.user: ~11 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`, `department_id`, `mobile`, `about`, `photo`, `user_type`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
  (1, 'John', 'Doe', 'admin@codekernel.net', '$2y$10$2lEkxufgGpCmXG2UY272WO/Cv6vna3TM4qP3rTYMCr7GcFu2Mhqyq', 0, '0123456789', 'I like to look at the clouds in the sky with a blank mind, I like to do thought experiment when I cannot sleep in the middle of the night.', NULL, 5, NULL, '2016-10-30 00:00:00', '2020-07-17 00:22:22', 1),
  (2, 'Wane', 'Willian', 'officer@codekernel.net', '$2y$10$U.JTKdWTWSRAw.H6Z.ZS3uJZrWaq3PssflkEe0xNW3ddNu5XS.rZe', 1, '0123456789', 'Checking the network cables, modem, and router. Reconnecting to Wi-Fi. Running Windows Network Diagnostics', NULL, 1, NULL, '2016-10-30 00:00:00', '2020-07-18 01:11:14', 1),
  (3, 'Xada', 'Roe', 'receptionist@codekernel.net', '$2y$10$FcsPFyCggD1kfn91WSDhSeqHc7n7j9X/u/Zbyn8kEx6qGjMn7mup2', 2, '0123456789', 'The Hello World project is a time-honored tradition in computer programming. It is a simple exercise that gets you started when learning something new. Let’s get started with GitHub!', NULL, 2, NULL, '2016-10-30 00:00:00', '2020-07-16 15:37:17', 1),
  (4, 'Jane', 'Doe', 'jane@doe.com', '$2y$10$Rpanf/X2B272cwTgjmKRMeqTlyham0iRu6WmFIAR4b6gaI2Mvh54m', 3, '0123456789', 'GitHub is a code hosting platform for version control and collaboration. It lets you and others work together on projects from anywhere.', NULL, 1, NULL, '2018-07-29 00:00:00', NULL, 1),
  (5, 'Annie', 'Smith', 'annie@example.com', '$2y$10$Rpanf/X2B272cwTgjmKRMeqTlyham0iRu6WmFIAR4b6gaI2Mvh54m', 3, '0123456789', 'Open this guide in a separate browser window (or tab) so you can see it while you complete the steps in the tutorial.', NULL, 1, NULL, '2018-07-29 00:00:00', NULL, 1),
  (6, 'Alex', 'Smith', 'alex@codekernel.net', '$2y$10$5DwvyIRa5P4CYhAhTQkjeu3BmX.J5sbokQQUuHh/O4pNUv02QvOKq', 4, '0123456789', 'A repository is usually used to organize a single project. Repositories can contain folders and files, images, videos, spreadsheets, and data sets – anything your project needs. ', NULL, 1, NULL, '2020-05-15 00:00:00', '2020-07-18 01:11:23', 1),
  (7, 'Bob', 'Banny', 'bob@codekernel.net', '$2y$10$Zfby6SvTitbJ0bO9CZI3GubPiMtM6T/Xv1VIsDJoyzgg.edxSyE8.', 5, '0123456789', 'Branching is the way to work on different versions of a repository at one time.', NULL, 1, NULL, '2020-05-15 00:00:00', NULL, 1),
  (8, 'Danniyel', 'Dan', 'dan@codekernel.net', '$2y$10$l09QqbcYQ3BXiiScfHlMHuhXJKbLm8GyZObj7SWJ6a3fSK7jwvp0O', 6, '0123456789', '', NULL, 1, NULL, '2020-05-15 00:00:00', NULL, 1),
  (9, 'Jennifer', 'Doe', 'jennifer@codekernel.net', '$2y$10$ztTEJRFdS42R9JueIEAgnumeH1Da99iWHGA5ove6zGjOxfDsTEEOe', 6, '0123456789', '', NULL, 1, NULL, '2020-05-15 00:00:00', NULL, 1),
  (10, 'Tylor', 'Ronnie', 'client@codekernel.net', '$2y$10$2lEkxufgGpCmXG2UY272WO/Cv6vna3TM4qP3rTYMCr7GcFu2Mhqyq', NULL, '0123456789', NULL, NULL, 3, NULL, '2020-05-15 00:00:00', NULL, 1);
 

-- Dumping structure for table db_queue_pda.user_social_accounts
DROP TABLE IF EXISTS `user_social_accounts`;
CREATE TABLE IF NOT EXISTS `user_social_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider_name` varchar(32) DEFAULT NULL,
  `provider_id` varchar(64) DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
-- Dumping structure for table db_queue_dev.website
DROP TABLE IF EXISTS `website`;
CREATE TABLE IF NOT EXISTS `website` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) DEFAULT NULL,
  `type` enum('section','slider','page') DEFAULT 'section',
  `title` varchar(128) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(128) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `position` tinyint(3) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue_dev.website: ~11 rows (approximately)
DELETE FROM `website`;
/*!40000 ALTER TABLE `website` DISABLE KEYS */;
INSERT INTO `website` (`id`, `menu`, `type`, `title`, `sub_title`, `description`, `image`, `url`, `position`, `status`, `created_at`, `updated_at`) VALUES
  (1, 'get-started', 'section', 'CodeKernel.Net', 'CodeKernel :: Token - Queue Management System', 'Queue Management System is a most powerful and easiest queue token system. It will help to manage customer/client waiting list and allocation processes in an organized way. It consist of Ticket is responsible for issuing the ticket, it can connect on a printer for printing, Service can have its ticket series/serial numbering with the department, counter and officer name.', NULL, NULL, 0, 1, '2020-09-16 14:01:43', '2020-09-17 08:07:33'),
  (2, 'explore', 'section', 'Discover your potential with us!', 'Our top instructors are waiting for you to step in so that they can upgrade your repertoire and unlock the doors to limitless possibilities for you.', NULL, NULL, NULL, 2, 1, '2020-09-16 16:28:49', '2020-09-17 22:14:45'),
  (3, 'about-us', 'section', 'CodeKernel - Software Labs', 'CodeKernel :: Token - Queue Management System', 'We build highly customizable and dynamic web based application using cutting edge technology such as HTML, CSS, Bootstrap 3/4.x, Raw PHP, Laravel 5/6/7/8.x, Codeigniter 2/3/4.x, Javascrpt, JSON and Jquery, Angular 7/8/9.x, MySQL 4/5.x\r\nMongoDB', '/public/assets/img/website/about.png', NULL, 3, 1, '2020-09-16 16:29:36', '2020-09-17 22:14:47'),
  (4, 'experts', 'section', 'Our Experts', NULL, 'Meet our experts! they will solve your issue within a few minutes!', NULL, NULL, 4, 1, '2020-09-16 16:29:55', '2020-09-17 22:14:48'),
  (5, 'contact-us', 'section', 'Get in Touch!', NULL, NULL, '/public/assets/img/website/map.png', NULL, 5, 1, '2020-09-16 16:30:19', '2020-09-17 22:14:49'),
  (6, 'slider-1', 'slider', 'Explore CodeKernel', 'CodeKernel :: Token - Queue Management System', 'Meet our experts! they will solve your issue within a few minutes!', '/public/assets/img/website/slide2.jpg', NULL, 1, 1, '2020-09-16 16:33:35', '2020-09-17 16:55:17'),
  (7, 'slider', 'slider', 'Token - Queue Management System', 'CodeKernel - Software Labs', '<span style="font-weight: bold; background-color: yellow;">We are experienced professionals in building websites, applications, and mobile solutions.</span>', '/public/assets/img/website/slide1.jpg', NULL, 0, 1, '2020-09-16 17:49:09', '2020-09-17 16:55:15'),
  (8, 'slider-2', 'slider', 'Token - Queue Management System', 'Token - Queue Management System', 'Token - Queue Management System', '/public/assets/img/website/slide3.jpg', 'Token - Queue Management System', 0, 1, '2020-09-16 22:46:40', '2020-09-17 21:49:41'),
  (9, 'codekernel', 'page', 'CodeKernel :: Token - Queue Management System', 'We build highly customizable and dynamic web based application using cutting edge technology such as HTML, CSS, Bootstrap 3/4.x, Raw PHP, Laravel 5/6/7/8.x, Codeigniter 2/3/4.x, Javascrpt, JSON and Jquery, Angular 7/8/9.x, MySQL 4/5.x\r\nMongoDB', 'We build highly customizable and dynamic web based application using cutting edge technology such as HTML, CSS, Bootstrap 3/4.x, Raw PHP, Laravel 5/6/7/8.x, Codeigniter 2/3/4.x, Javascrpt, JSON and Jquery, Angular 7/8/9.x, MySQL 4/5.x\r\nMongoDB', '/public/assets/img/website/about.png', 'http://codekernel.net', 0, 1, '2020-09-17 16:51:12', '2020-09-28 13:13:55'),
  (10, 'other', 'section', 'CodeKernel :: Token - Queue Management System', 'We build highly customizable and dynamic web based application using cutting edge technology such as HTML, CSS, Bootstrap 3/4.x, Raw PHP, Laravel 5/6/7/8.x, Codeigniter 2/3/4.x, Javascrpt, JSON and Jquery, Angular 7/8/9.x, MySQL 4/5.x\r\nMongoDB', 'We build highly customizable and dynamic web based application using cutting edge technology such as HTML, CSS, Bootstrap 3/4.x, Raw PHP, Laravel 5/6/7/8.x, Codeigniter 2/3/4.x, Javascrpt, JSON and Jquery, Angular 7/8/9.x, MySQL 4/5.x\r\nMongoDB', '/public/assets/img/website/about.png', 'http://codekernel.net', 1, 0, '2020-09-17 22:12:48', '2020-09-20 15:28:46');
/*!40000 ALTER TABLE `website` ENABLE KEYS */;


-- Dumping structure for table db_queue.display_custom
DROP TABLE IF EXISTS `display_custom`;
CREATE TABLE IF NOT EXISTS `display_custom` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `counters` varchar(64) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1-active, 2-inactive',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table db_queue.display_custom: ~0 rows (approximately)
DELETE FROM `display_custom`; 
INSERT INTO `display_custom` (`id`, `name`, `description`, `counters`, `status`, `created_at`, `updated_at`) VALUES
  (1, 'Floor 1', 'TEST 1', '1,2,3,6', 1, '2020-10-01 11:34:44', '2020-10-01 22:40:10'),
  (2, 'Floor 2', 'TEST 2', '6,7,8,9,10', 0, '2020-10-01 11:35:28', '2020-10-01 17:17:20'),
  (3, 'Floor 3', 'TEST 3', '8,9,10,11,12,13', 1, '2020-10-01 11:35:51', '2020-10-01 16:48:36'),
  (4, 'Floor 4', 'TESTS Floor', '4,5,6,7', 1, '2020-10-01 18:11:00', '2020-10-01 14:58:27');
 

DROP TABLE IF EXISTS `telescope_entries`;
CREATE TABLE IF NOT EXISTS `telescope_entries` (
  `sequence` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` CHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
  `batch_id` CHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
  `family_hash` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
  `should_display_on_index` TINYINT(1) NOT NULL DEFAULT '1',
  `type` VARCHAR(20) NOT NULL COLLATE 'utf8mb4_unicode_ci',
  `content` LONGTEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
  `created_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  UNIQUE INDEX `telescope_entries_uuid_unique` (`uuid`),
  INDEX `telescope_entries_batch_id_index` (`batch_id`),
  INDEX `telescope_entries_family_hash_index` (`family_hash`),
  INDEX `telescope_entries_created_at_index` (`created_at`),
  INDEX `telescope_entries_type_should_display_on_index_index` (`type`, `should_display_on_index`)
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;


DROP TABLE IF EXISTS `telescope_entries_tags`;
CREATE TABLE IF NOT EXISTS `telescope_entries_tags` (
  `entry_uuid` CHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
  `tag` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
  INDEX `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`, `tag`),
  INDEX `telescope_entries_tags_tag_index` (`tag`),
  CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

DROP TABLE IF EXISTS `telescope_monitoring`;
CREATE TABLE IF NOT EXISTS `telescope_monitoring` (
  `tag` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci'
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;
