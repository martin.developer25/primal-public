<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

# -----------------------------------------------------------
# WEBSITE
# -----------------------------------------------------------
Route::namespace('App\Http\Controllers\Frontend')
    ->group(function() {
    // webiste
    Route::get('/', 'FrontendController@index')->name('/');
    Route::get('page', 'FrontendController@index');
    Route::get('page/{page}', 'FrontendController@index');
 
    # -----------------------------------------------------------
    # CLIENT
    # -----------------------------------------------------------  
    Route::name('client.')
        ->prefix('client') 
        ->group(function() { 
        // auth
        Route::post('signin', 'AuthController@signin')->name('signin');
        Route::post('signup', 'AuthController@signup')->name('signup'); 
        Route::get('signout', 'AuthController@signout')->name('signout');
        Route::get('account-confirmation/{token}/', 'AuthController@accountConfirmation')->name('account-confirmation');
        Route::post('forgot-password', 'AuthController@forgotPassword')->name('forgot-password');
        // # login - {provider: google}
        // Route::get('login/{provider}', 'FrontendController@redirectToProvider');
        // Route::get('login/{provider}/callback', 'FrontendController@handleProviderCallback'); 

        # -----------------------------------------------------------
        # AUTHORIZED 
        # -----------------------------------------------------------
        Route::middleware(['auth', 'roles:client'])
            ->group(function() { 

            Route::get('/', 'ClientController@signin')->name('dashboard');
            Route::get('dashboard', 'ClientController@dashboard')->name('dashboard');
            Route::get('profile', 'ClientController@profile')->name('profile');
            Route::put('profile', 'ClientController@profileUpdate')->name('profile');
            Route::get('token', 'ClientController@token')->name('token');
            Route::post('token', 'ClientController@tokenGenerate')->name('token');
            Route::get('token/report', 'ClientController@tokenReport')->name('report');
            Route::post('token/data', 'ClientController@tokenData')->name('token.data');
            Route::post('token/print', 'ClientController@tokenShow')->name('token.print');
            Route::get('token/stoped/{id}', 'ClientController@stoped');
            Route::post('token/generate', 'ClientController@generate')->name('token.generate');
        });
    });
});

# -----------------------------------------------------------
# BACKENDLOGIN
# -----------------------------------------------------------
# login
Route::get('auth', 'App\Http\Controllers\Common\LoginController@login')->name('auth.login');
Route::get('auth/login', 'App\Http\Controllers\Common\LoginController@login')->name('auth.login');
Route::post('auth/login', 'App\Http\Controllers\Common\LoginController@checkLogin');
Route::get('auth/logout', 'App\Http\Controllers\Common\LoginController@logout')->name('auth.logout');


# -----------------------------------------------------------
# CLEAN CACHE
# -----------------------------------------------------------
Route::get('clean', function () {
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    // \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('clear-compiled');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    dd('Cached Cleared');
});


# -----------------------------------------------------------
# BACKEND SYSTEM USER COMMON 
# -----------------------------------------------------------
Route::prefix('common')
    ->namespace('App\Http\Controllers\Common')
    ->group(function() { 
    # switch language
    Route::get('language/{locale?}', 'LanguageController@index');

    # cron job
    Route::get('jobs', 'CronjobController@jobs');

    # display 
    Route::get('display','DisplayController@display');  
    Route::post('display1', 'DisplayController@display1');  
    Route::post('display2','DisplayController@display2');  
    Route::post('display3','DisplayController@display3'); 
    Route::post('display4','DisplayController@display4'); 
    Route::post('display5','DisplayController@display5'); 

    # -----------------------------------------------------------
    # AUTHORIZED COMMON 
    # -----------------------------------------------------------
    Route::middleware('auth') 
        ->group(function() { 
        #message notification
        Route::get('message/notify','NotificationController@message'); 
        # message  
        Route::get('message','MessageController@show'); 
        Route::post('message','MessageController@send'); 
        Route::get('message/inbox','MessageController@inbox'); 
        Route::post('message/inbox/data','MessageController@inboxData'); 
        Route::get('message/sent','MessageController@sent'); 
        Route::post('message/sent/data','MessageController@sentData'); 
        Route::get('message/details/{id}/{type}','MessageController@details'); 
        Route::get('message/delete/{id}/{type}','MessageController@delete');  
        Route::post('message/attachment','MessageController@UploadFiles'); 

        # profile 
        Route::get('setting/profile','ProfileController@profile');
        Route::get('setting/profile/edit','ProfileController@profileEditShowForm');
        Route::post('setting/profile/edit','ProfileController@updateProfile');
    });
});

# -----------------------------------------------------------
# BACKEND AUTHORIZED
# -----------------------------------------------------------
Route::group(['middleware' => ['auth']], function() {

    # -----------------------------------------------------------
    # ADMIN
    # -----------------------------------------------------------
    Route::prefix('admin')
        ->namespace('App\Http\Controllers\Admin')
        ->middleware('roles:admin')
        ->group(function() { 
        # home
        Route::get('/', 'HomeController@home');

        # user 
        Route::get('user', 'UserController@index');
        Route::post('user/data', 'UserController@userData');
        Route::get('user/create', 'UserController@showForm');
        Route::post('user/create', 'UserController@create');
        Route::get('user/view/{id}','UserController@view');
        Route::get('user/edit/{id}','UserController@showEditForm');
        Route::post('user/edit','UserController@update');
        Route::get('user/delete/{id}','UserController@delete');

        # department
        Route::get('department','DepartmentController@index');
        Route::get('department/create','DepartmentController@showForm');
        Route::post('department/create','DepartmentController@create');
        Route::get('department/edit/{id}','DepartmentController@showEditForm');
        Route::post('department/edit','DepartmentController@update');
        Route::get('department/delete/{id}','DepartmentController@delete');

        # counter
        Route::get('counter','CounterController@index');
        Route::get('counter/create','CounterController@showForm');
        Route::post('counter/create','CounterController@create');
        Route::get('counter/edit/{id}','CounterController@showEditForm');
        Route::post('counter/edit','CounterController@update');
        Route::get('counter/delete/{id}','CounterController@delete');

        # sms
        Route::get('sms/new', 'SmsSettingController@form');
        Route::post('sms/new', 'SmsSettingController@send');
        Route::get('sms/list', 'SmsSettingController@show');
        Route::post('sms/data', 'SmsSettingController@smsData');
        Route::get('sms/delete/{id}', 'SmsSettingController@delete');
        Route::get('sms/setting', 'SmsSettingController@setting');
        Route::post('sms/setting', 'SmsSettingController@updateSetting');

        # mail
        Route::get('mail/new', 'MailSettingController@form');
        Route::post('mail/new', 'MailSettingController@send');
        Route::get('mail/list', 'MailSettingController@show');
        Route::get('mail/contract', 'MailSettingController@contract');
        Route::post('mail/data', 'MailSettingController@mailData');
        Route::get('mail/delete/{id}', 'MailSettingController@delete');
        Route::get('mail/setting', 'MailSettingController@setting');
        Route::post('mail/setting', 'MailSettingController@updateSetting');

        # token
        Route::get('token/setting','TokenController@tokenSettingView'); 
        Route::post('token/setting','TokenController@tokenSetting'); 
        Route::get('token/setting/delete/{id}','TokenController@tokenDeleteSetting');
        Route::post('token/autoview','TokenController@tokenAutoView'); 
        Route::post('token/auto','TokenController@tokenAuto'); 
        Route::get('token/current','TokenController@current');
        Route::get('token/report','TokenController@report');  
        Route::post('token/report/data','TokenController@reportData');  
        Route::get('token/performance','TokenController@performance');  
        Route::get('token/create','TokenController@showForm');
        Route::post('token/create','TokenController@create');
        Route::post('token/print', 'TokenController@viewSingleToken');
        // Route::get('token/complete/{id}','TokenController@complete');
        Route::get('token/stoped/{id}','TokenController@stoped');
        Route::get('token/recall/{id}','TokenController@recall');
        Route::get('token/delete/{id}','TokenController@delete');
        Route::post('token/transfer','TokenController@transfer');
        Route::get('token/complete','TokenController@tokenPhone'); 

        # setting
        Route::get('setting','SettingController@showForm'); 
        Route::post('setting','SettingController@update');  
        Route::post('setting/frontend','SettingController@frontend');
        Route::get('setting/website/content/{id}','SettingController@getContent');  
        Route::post('setting/website/content','SettingController@storeContent');  
        Route::get('setting/website/slug','SettingController@pageSlug');  
        Route::get('setting/display','DisplayController@showForm');  
        Route::post('setting/display','DisplayController@setting'); 
        Route::get('setting/display/custom','DisplayController@getCustom');  
        Route::post('setting/display/custom','DisplayController@custom');  
    });

    # -----------------------------------------------------------
    # OFFICER
    # -----------------------------------------------------------
    Route::prefix('officer')
        ->namespace('App\Http\Controllers\Officer')
        ->middleware('roles:officer')
        ->group(function() { 
        # home
        Route::get('/', 'HomeController@home');
        # user
        Route::get('user/view/{id}', 'UserController@view');

        # token
        Route::get('token','TokenController@index');
        Route::post('token/data','TokenController@tokenData');  
        Route::get('token/current','TokenController@current');
        Route::get('token/complete/{id}','TokenController@complete');
        Route::get('token/recall/{id}','TokenController@recall');
        Route::get('token/stoped/{id}','TokenController@stoped');
        Route::post('token/print', 'TokenController@viewSingleToken');
    });

    # -----------------------------------------------------------
    # RECEPTIONIST
    # -----------------------------------------------------------
    Route::prefix('receptionist')
        ->namespace('App\Http\Controllers\Receptionist')
        ->middleware('roles:receptionist')
        ->group(function() { 
        # home
        Route::get('/','TokenController@tokenAutoView'); 

        # token
        Route::get('token/auto','TokenController@tokenAutoView'); 
        Route::post('token/auto','TokenController@tokenAuto'); 
        Route::get('token/create','TokenController@showForm');
        Route::post('token/create','TokenController@create');
        Route::get('token/current','TokenController@current'); 
        Route::post('token/print', 'TokenController@viewSingleToken');
    }); 
});
