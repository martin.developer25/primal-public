<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\Template;
use Mail;

class MailJob implements ShouldQueue
{
    /*
    |   use App\Jobs\MailJob;
    |
    |   MailJob::dispatch($email, $subject, $message) 
    |       ->delay(now()->addSeconds(30)); 
    |----------------------------------------------------------
    |*/

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels; 

    protected $email;
    protected $subject;
    protected $message;
    /**
     * Create a new job instance. 
     * @return void
     */
    public function __construct(string $email = null, string $subject = null, string $message = null)
    {
        $this->email   = $email;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        Mail::to($this->email)
            ->send(new Template($this->subject, $this->message));
    }
}
