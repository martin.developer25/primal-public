<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Common\MailController;
use App\Models\MailHistory;
use App\Models\Token; 
use Illuminate\Support\Facades\Log;

class MailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->mail();

        return 0;
    }

    /*
    *---------------------------------------------------------
    * SEND MAIL
    *--------------------------------------------------------- 
    */

    public function mail()
    { 
        date_default_timezone_set(session('app.timezone') ?? "Asia/Dhaka");
        /*
        * First priority status = 2
        * Schedule Time Wise
        *---------------------------------------------------
        */  
        $this->info("START: mail initiating...");
        $mails = MailHistory::where("status", 0)  
                ->orderBy('status', 'DESC')
                ->limit(10)
                ->get();

        $this->info("data fetching..."); 

        $success = 0;
        $failed  = 0;
        foreach ($mails as $mail)  { 
            try {
                (new MailController)->retry([
                    'id'      => $mail->id,
                    'email'   => $mail->email,
                    'subject' => $mail->subject,
                    'message' => $mail->message
                ]); 

                $this->info("success {".$success++."}: [{$mail->email}] {$mail->subject}");
            } catch(Exception $e) {
                $this->info("failed  {".$failed++."}: [{$mail->email}] {$mail->subject}");
                Log::error("MailCommand::mail - failed  {".$failed++."}: [{$mail->email}] {$mail->subject}");
            }
        }  

        $this->info("END: job done! total success {$success} and failed {$failed}");
    }

}
