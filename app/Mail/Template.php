<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB, Config, Session;

class Template extends Mailable
{
    use Queueable, SerializesModels;
  
    public $app = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sub = null, $msg = null)
    {   
        $this->app = DB::table('setting')->first();
        $this->mail = DB::table('mail_setting')->first();
        $this->app->subject = (!empty($sub)?$sub:$app->title);
        $this->app->message = $msg; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        return $this->view('backend.common.mail.template')
            ->from($this->app->email, str_replace([" ", "_"], "_", $this->app->title))
            ->subject($this->app->subject)
            ->with([
              'app' => $this->app 
            ]);
    }
}
