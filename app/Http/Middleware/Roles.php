<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Roles
{ 
    public function handle($request, Closure $next, $roles)
    {
        $roles = array_slice(func_get_args(), 2);
        $auth  = false;

        if (empty($roles))
        {  
            $auth  = false;
        } 
        else
        {
            foreach ($roles as $role) 
            {
                try 
                {   
                    if (Auth::check() && Auth::user()->hasRole($role)) 
                    { 
                        $auth  = true;
                        return $next($request);
                    }
                    else
                    {
                        $auth  = false;
                    } 
                } 
                catch (ModelNotFoundException $exception) 
                {
                    $auth  = false;
                    dd('Could not find role ' . $role);
                }
            } 

            $auth  = false;
        }   

        if ($auth == false)
        { 
            if (request()->is('client/*'))
            {
                return redirect()->route('/')->with('exception', trans('app.you_are_not_authorized'));
            }
            else
            { 
                return redirect()->route('auth.login')
                ->with('exception', trans('app.you_are_not_authorized'));
            } 
        }
    }
}

 