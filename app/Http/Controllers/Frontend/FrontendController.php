<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;  

use App\Models\Setting; 
use App\Models\Website; 
use App\Models\User;
use App\Models\Token;
use App\Models\UserSocialAccount;
use Session;

class FrontendController extends Controller
{   
    public function index(Request $request)
    {
        $app  = Setting::first();
        if (!$app->website) 
        {
            return redirect('auth');
        }
        $contents = $this->contents();

        if (!empty($contents->page->{$request->page})) 
        { 
            $page = $contents->page->{$request->page};
            return view('frontend.page.page', compact(
                'app',
                'page',
                'contents'
            ));  
        } 
        else 
        {
            $officers = User::where('status', 1)
                ->whereIn('user_type', [1,5])
                ->get();
            $counters = $this->counters();

            return view('frontend.page.home', compact(
                'app',
                'contents',
                'officers',
                'counters'
            )); 
        }
    }
  
    public function contents()
    {
        $contents = Website::where('status', 1)
            ->orderByRaw("FIELD(type, 'slider', 'section', 'page') ASC") 
            ->orderBy('position', 'ASC')
            ->orderBy('menu', 'ASC')
            ->get();

        $menus    = [];
        $sliders  = [];
        $sections = [];
        $page     = []; 

        foreach ($contents as $content) 
        { 
            $data = (object)[
                "menu"        => ucfirst(str_replace('-', ' ', $content->menu)),
                "title"       => $content->title,
                "sub_title"   => $content->sub_title,
                "description" => $content->description,
                "image"       => asset($content->image),
                "url"         => $content->url
            ];

            if ($content->type  == 'slider')
            {
                $menus["#home"] = "Home";
                $sliders[] = $data;
            }
            else if ($content->type  == 'section')
            {
                $menus["#{$content->menu}"] = ucfirst(str_replace('-', ' ', $content->menu));
                $sections[$content->menu] = $data;
            }
            else if ($content->type  == 'page')
            {
                $menus[url("page/{$content->menu}")] = ucfirst(str_replace('-', ' ', $content->menu));
                $page[$content->menu]  = $data;
            }
        }

        \Session::put('menus', $menus);

        return (object)[
            'menus'    => (object)$menus,
            'sliders'  => (object)$sliders,
            'sections' => (object)$sections,
            'page'     => (object)$page
        ];
    }

    public function counters()
    {
        $infobox = array();
        $tokens  = Token::selectRaw("
                COUNT(CASE WHEN status = '0' THEN id END) AS pending,
                COUNT(CASE WHEN status = '1' THEN id END) AS complete,
                COUNT(CASE WHEN status = '2' THEN id END) AS stop,
                COUNT(id) AS total
            ")
            ->first();
        $infobox['Clients']        = User::where('user_type', 3)->count();
        $infobox['Total Token']    = $tokens->total;
        $infobox['Total Pending']  = $tokens->pending;
        $infobox['Total Complete'] = $tokens->complete;

        return $infobox; 
    } 
}
