<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Laravel\Socialite\Facades\Socialite;

use App\Http\Controllers\Common\MailController AS MailController;
use App\Models\Setting; 
use App\Models\Website; 
use App\Models\User;
use App\Models\UserSocialAccount;
use Session, Auth, Validator, Hash; 

class AuthController extends Controller
{   
    use ThrottlesLogins; 
     
    public function signin(Request $request)
    {
        //start throttoling
        if ($this->hasTooManyLoginAttempts($request)) 
        {
            $this->fireLockoutEvent($request);
            $data = [
                'status'  => false,
                'message' => trans('app.to_many_login_attempts'),
                'data'    => []
            ]; 
        } 
        else
        { 
            $validator = Validator::make($request->all(), [ 
                'email'       => 'required|email', 
                'password'    => 'required'
            ])
            ->setAttributeNames(array(
               'email'     => trans('app.email'), 
               'password'  => trans('app.password')
            ));


            if ($validator->fails()) 
            {
                $resError = [];
                foreach ($validator->errors()->messages() as $key => $value) 
                {
                    $resError[$key] = $value[0];
                }

                return response([
                    'status'  => false,
                    'message' => trans('app.invalid_credential'),
                    'data'    => $resError
                ]);
            } 
            else 
            {
                if (\Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'user_type'=> '3'])) 
                {
                    $authData = Auth::user();
                    unset($authData->password);   

                    if ($authData->status == '0')
                    { 
                        $data = [
                            'status'  => false,
                            'message' => trans('app.your_account_is_not_confirm_yet'),
                            'data'    => []
                        ];  
                    }
                    else if ($authData->status == '2')
                    {
                        $data = [
                            'status'  => false,
                            'message' => trans('app.contact_with_authurity'),
                            'data'    => []
                        ];   
                    }
                    else 
                    {  
                        Auth::login($authData, true); 
                        $data = [
                            'status'  => true,
                            'message' => trans('app.signin_successful'),
                            'data'    => []
                        ];    
                    }
                }
                else
                {
                    $data = [
                        'status'  => false,
                        'message' => trans('app.invalid_credential'),
                        'data'    => []
                    ];   
                }
            } 
        }


        $this->incrementLoginAttempts($request);
        // update throttoling 

        return response()->json($data);
    }

    public function signup(Request $request)
    {   
        $validator = Validator::make($request->all(), [ 
            'email'       => 'required|unique:user,email|max:50',
            'firstname'   => 'required|max:25',
            'lastname'    => 'required|max:25',
            'password'    => 'required|min:4|max:50',
            'conf_password' => 'required|same:password'
        ])
        ->setAttributeNames(array(
           'email'     => trans('app.email'),
           'firstname' => trans('app.firstname'),
           'lastname'  => trans('app.lastname'),
           'password'  => trans('app.password'),
           'conf_password' => trans('app.conf_password')
        ));   


        if ($validator->fails()) 
        {
            $resError = [];
            foreach ($validator->errors()->messages() as $key => $value) 
            {
                $resError[$key] = $value[0];
            }

            return response([
                'status'  => false,
                'message' => trans('app.validation_failed'),
                'data'    => $resError
            ]);
        } 
        else 
        { 
            $token = base64_encode($request->email.'#'.uniqid());
            $token = strrev($token);
            $token = str_replace("==", "", $token);
            $link  = url('client/account-confirmation/'.$token);
            $save = User::insert([ 
                'firstname'   => $request->firstname,
                'lastname'    => $request->lastname,
                'email'       => $request->email,
                'password'    => Hash::make($request->conf_password), 
                'department_id' => null,
                'mobile'      => null, 
                'user_type'   => 3, // client  
                'remember_token' => $token,
                'created_at'  => date('Y-m-d H:i:s'),
                'status'      => 0,
            ]); 

            if ($save) 
            {      
                try {
                    (new MailController)->send([
                        'email'   => $request->email,
                        'subject' => trans('app.account_confirmation_mail'). (session()->has('app.title')?(' :: '. session()->get('app.title')):null),
                        'message' => "<h1 style='color: #153643; font-family: Arial, sans-serif; font-size: 24px;'>Thank you {$request->firstname} {$request->lastname} for signing up.</h1>  
                            <p>Before we can complete your request, we need to validate e-mail address.</p>
                            <p>Click the link bellow to continue, or paste the following address in your web browser.</p>
                            </p><a href='{$link}'>{$link}</a></p>
                            <br/>
                            <br/>
                            <p style='margin-bottom:0;padding-bottom:0'>Thanking you,</p>"
                    ]);

                    return response([
                        'status'  => true,
                        'message' => trans('app.signup_successfully'),
                        'data'    => []
                    ]);  

                } catch(Exception $e) {
                    return response([
                        'status'  => true,
                        'message' => trans('app.signup_successfully') . ' Confirmation mail not sent to you, please contract with the author!',
                        'data'    => []
                    ]);  
                }
            } 
            else 
            {
                return response([
                    'status'  => false,
                    'message' => trans('app.please_try_again'),
                    'data'    => []
                ]); 
            } 
        }
    }
 
    public function signout()
    { 
        Session::flush();
        Auth::logout();        
        return redirect('/')->with('message', trans('app.signout_successfully'));
    }

    //prerequisite for login throttling
    public function username()
    {
        return 'email';
    } 

    public function accountConfirmation(Request $request)
    { 
        if (!empty($request->token))
        {
            $token = base64_decode(strrev($request->token));
            $email = explode("#", $token); 
            
            if (!empty($email[0]) && filter_var($email[0], FILTER_VALIDATE_EMAIL))
            {
                $exists = User::whereNull('updated_at')
                    ->where('status', '0')
                    ->where('user_type', '3')
                    ->where('email', $email[0])
                    ->where('remember_token', $request->token)
                    ->exists();

                if ($exists)
                {
                    $update = User::where('email', $email[0])
                        ->where('user_type', '3')
                        ->update([
                            'status' => '1',
                            'remember_token' => null,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]); 

                    if ($update)
                    {
                        $data = [
                            'status'   => true,
                            'title'    => 'Account verification!',  
                            'message'  => 'Verification successful! Congratulations, your account has been successfully created. Please signin to start your session.', 
                        ];
                    }
                    else
                    {
                        $data = [
                            'status'   => false,
                            'title'    => 'Account verification!',  
                            'message'  => 'Internal server error! Failed to complete the proccess! please try again later.', 
                        ]; 
                    } 
                }
                else
                { 
                    $data = [
                        'status'   => false,
                        'title'    => 'Account verification!', 
                        'message'  => 'Invalid token! The token has been expired.', 
                    ]; 
                } 
            }
            else
            {
                $data = [
                    'status'   => false,
                    'title'    => 'Account verification!',  
                    'message'  => 'Invalid token! The token has been expired.', 
                ];  
            }
        }
        else
        {
            $data = [
                'status'   => false,
                'title'    => 'Account verification!',  
                'message'  => 'Invalid token! The token not found!', 
            ];  
        } 

        return view('frontend.page.message')->with($data);
    }


    public function forgotPassword(Request $request)
    {    
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email'
        ])
        ->setAttributeNames(array(
           'email'  => trans('app.email')
        ));   
  
        if ($validator->fails()) 
        {
            $resError = [];
            foreach ($validator->errors()->messages() as $key => $value) 
            {
                $resError[$key] = $value[0];
            }

            return response([
                'status'  => false,
                'message' => trans('app.validation_failed'),
                'data'    => $resError
            ]);
        } 
        else 
        {    
            $user = User::where('email', $request->email)
                ->where('status', 1)
                ->first();

            if (!empty($user))
            { 
                $password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+-/[]{}') , 0 , 10 );
                $hash     = Hash::make($password); 
              
                User::where('email', $request->email) 
                ->update(['password' => $hash]);
 
                (new MailController)->send([
                    'email'   => $user->email,
                    'subject' => trans('app.forgot_password'). (session()->has('app.title')?(' :: '. session()->get('app.title')):null),
                    'message' => "<h1 style='color: #153643; font-family: Arial, sans-serif; font-size: 24px;'>Hello, {$user->firstname} {$user->lastname}.</h1>  
                        <p>You have requested a password reset, we have received the request and generate a new password for you.</p>
                        <p>The new password has been given below.</p>
                        <p style='background:gold;padding:10px;color:#000;'>{$password}</p>
                        <br/>
                        <br/>
                        <p style='margin-bottom:0;padding-bottom:0'>Thanking you,</p>"
                ]);
            } 
   
            return response([
                'status'  => true,
                'message' => "If an account matches {$request->email}, you should receive an email of reset password shortly.",
                'data'    => []
            ]); 
        }
    }
    /*----------------------------------------------------------------
    |--------GOOGLE AUTHENTICATION------------------------------------
    |----------------------------------------------------------------*/
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {
        try {
            return Socialite::driver($request->provider)->redirect();
        } catch (Exception $e) {
            return redirect('client.signin')->with('message', trans('app.invalid_credential'));
        }
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
        try { 
            $user = Socialite::driver($request->provider)->user();
        } catch (Exception $e) {
            return redirect('client.signin')->with('exception', trans('app.invalid_credential'));
        }

        $authUser = $this->findOrCreateUser($user, $request->provider);
 
        if ($authData->status == '0')
        { 
            $data = [
                'status'  => false,
                'message' => trans('app.your_account_is_not_confirm_yet'),
                'data'    => null
            ];  
        }
        else if ($authData->status == '2')
        {
            $data = [
                'status'  => false,
                'message' => trans('app.contact_with_authurity'),
                'data'    => null
            ];   
        }
        else 
        {  
            Auth::login($authData, true); 
            $data = [
                'status'  => true,
                'message' => trans('app.signin_successful'),
                'data'    => null
            ];    
        }
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $user
     * @return User
     */
    public function findOrCreateUser($providerUser = [], $provider = null)
    {
        $existsAccount = UserSocialAccount::where('provider_name', $provider)
                   ->where('provider_id', $providerUser->getId())
                   ->first();

        if ($existsAccount) 
        { 
            return $existsAccount->user()->first();
        } 
        else 
        {
            $user = User::where('email', $providerUser->getEmail())->first();

            if (! $user) {
                $user = User::create([  
                    'firstname' => $providerUser->getName(),
                    'lastname'  => $providerUser->getNickname(),
                    'email'     => $providerUser->getEmail(),
                    'password'  => Hash::make($providerUser->getId()), 
                    'photo'     => $providerUser->getAvatar(),
                    'user_type' => '3', // client
                    'created_at' => date('Y-m-d H:i:s'),
                    'status'    => '1',
                ]);
            }

            $user->accounts()->create([
                'user_id'       => $user->id,
                'provider_id'   => $providerUser->getId(),
                'provider_name' => $provider,
                'created_at'    => date('Y-m-d H:i:s'),
            ]);
 
            return $user;
        }
    }

}


