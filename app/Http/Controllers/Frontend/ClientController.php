<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

use App\Http\Controllers\Common\Token_lib AS Token_lib;
use App\Http\Controllers\Common\DisplaySetting AS DSetting;
use App\Models\Setting;  
use App\Models\DisplaySetting;  
use App\Models\TokenSetting;  
use App\Models\Token; 
use App\Models\User;  
use App\Models\Department;
use App\Models\Counter;
use DB, Validator, Hash;

class ClientController extends Controller
{  

    # -----------------------------------------------------------
    # PROFILE 
    # -----------------------------------------------------------
     
    public function profile(Request $request)
    {
        $app  = Setting::first();
        $display = DisplaySetting::first(); 
        $user = User::find(auth()->user()->id);

        return view('frontend.client.profile', compact(
            'app',
            'display',
            'user'
        ));  
    }


    public function profileUpdate(Request $request)
    {    
        $validator = Validator::make($request->all(), [ 
            'email'       => 'required|max:50|unique:user,email,' . auth()->user()->id,
            'firstname'   => 'required|max:25',
            'lastname'    => 'required|max:25',
            'password'    => 'required|max:50',
            'conf_password' => 'required|max:50|same:password', 
            'mobile'      => 'max:20'.(!empty($request->mobile_required)?'|required':''),
            'photo'       => 'image|mimes:jpeg,png,jpg,gif|max:3072',
            'about'       => 'max:512',
        ])
        ->setAttributeNames(array(
           'email'         => trans('app.email'),
           'firstname'     => trans('app.firstname'),
           'lastname'      => trans('app.lastname'),
           'password'      => trans('app.password'),
           'conf_password' => trans('app.conf_password'),
           'about'         => trans('app.about'),
           'mobile'        => trans('app.mobile'),
           'photo'         => trans('app.photo'),
           'about'         => trans('app.about'),
        )); 

 
        $photo = $request->file('photo');
        $photoPath = $request->old_photo;
        if ($request->hasFile('photo') && $photo->isValid())
        {
            $fileName = uniqid('u_', true).time().bin2hex(random_bytes(5)).'.'.$photo->getClientOriginalExtension();
            $photoPath = 'public/assets/img/users/'.$fileName;
            $photo->storeAs('/img/users/', $fileName);
        }

        if ($validator->fails()) 
        {  
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('photo', $photoPath);
        } 
        else 
        {  

            $update = User::where('id', auth()->user()->id )
            ->update([  
                'firstname'   => $request->firstname,
                'lastname'    => $request->lastname,
                'email'       => $request->email,
                'password'    => Hash::make($request->conf_password),
                'mobile'      => $request->mobile,
                'about'       => $request->about,
                'photo'       => $photoPath,
                'updated_at'  => date('Y-m-d'),
            ]);

            if ($update) 
            { 
                return redirect()
                    ->back()
                    ->withInput()  
                    ->with('photo', $photoPath)
                    ->with('message', trans('app.update_successfully'));
            } 
            else 
            {
                return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with('photo', $photoPath)
                    ->with('exception', trans('app.please_try_again'));
            }

        } 
    }

    /*-----------------------------------
    | TOKEN
    |-----------------------------------*/
    public function token()
    {
        $app  = Setting::first();
        date_default_timezone_set($app->timezone);
        $tokens = $this->tokenManager();
 
        return view('frontend.client.token', compact(
            'app', 
            'tokens' 
        )); 
    }

    public function tokenManager()
    {
        $display = DisplaySetting::first(); 

        $tokensRaw = Token::where('created_by', auth()->user()->id)
            ->whereDate('created_at', date('Y-m-d'))
            ->whereIn('status', ['0','2'])
            ->get();
            
        $tokens = (object)[
            'display' => $display,
            'list'    => [],
            'total'   => 0,
            'pending' => false,
            'stoped'  => false,
            'success' => false
        ];


        $pendingCount = 0;
        $stopedCount  = 0; 
        $successCount = 0; 
        foreach($tokensRaw as $t)
        {      
             
            // manage waiting position
            $position = 0;
            if ($t->status==0)
            if ($display->display==1)
            {
                $all = Token::where('status', '0')
                    ->whereDate('created_at', date('Y-m-d'))
                    ->get();

                $counter = 1;
                foreach($all as $st)
                {
                    if ($st->id != $t->id)
                    {
                        $counter++; 
                    }
                    else
                    {
                        break;
                    } 
                } 
                $position = $counter;
            }
            else if (in_array($display->display, array(2,3,5,6)))
            {
                $all = Token::where('status', '0')
                    ->where('counter_id', $t->counter_id)
                    ->whereDate('created_at', date('Y-m-d'))
                    ->get();

                $counter = 1;
                foreach($all as $st)
                {
                    if ($st->id != $t->id)
                    {
                        $counter++; 
                    }
                    else
                    {
                        break;
                    } 
                } 
                $position = $counter;
            }
            else if ($display->display==4)
            {
                $all = Token::where('status', '0')
                    ->where('department_id', $t->department_id)
                    ->whereDate('created_at', date('Y-m-d'))
                    ->get();

                $counter = 1;
                foreach($all as $st)
                {
                    if ($st->id != $t->id)
                    {
                        $counter++; 
                    }
                    else
                    {
                        break;
                    } 
                } 
                $position = $counter;
            }
            $t->position = ($position==1)?"Now serving":($position==0?"":"Waiting ".($position-1));


            // manage status
            if ($t->status==0)
            {
                $pendingCount++;
                if ($pendingCount >= 3) 
                { 
                    $tokens->pending = true; 
                } 
                if ($stopedCount < 3) 
                { 
                    $stopedCount = 0; 
                } 
            }
            else if ($t->status==1)
            {
                $successCount++;
                if ($stopedCount < 3) 
                { 
                    $stopedCount = 0; 
                } 
            }
            else if ($t->status==2)
            {
                $stopedCount++;
                if ($stopedCount >= 3) 
                { 
                    $tokens->stoped = true; 
                } 
            } 

            // at least success 1 token in 10
            if ($tokens->total>=9 && $successCount == 0) 
            {  
                $tokens->success = true; 
            } 

            $tokens->list[] = $t;
            $tokens->total = $tokens->total+1; 
        }  
 
        if ($display->display == 5)
        {
            $tokens->tokenButton = TokenSetting::select( 
                    'department.name',
                    'token_setting.department_id',
                    'token_setting.counter_id',
                    'token_setting.user_id',
                    DB::raw('CONCAT(user.firstname ," " ,user.lastname) AS officer')
                )
                ->join('department', 'department.id', '=', 'token_setting.department_id')
                ->join('counter', 'counter.id', '=', 'token_setting.counter_id')
                ->join('user', 'user.id', '=', 'token_setting.user_id')
                ->where('token_setting.status',1)
                ->groupBy('token_setting.user_id')
                ->orderBy('token_setting.department_id', 'ASC')
                ->get();
        }
        else
        {
            $tokens->tokenButton = TokenSetting::select( 
                    'department.name',
                    'token_setting.department_id',
                    'token_setting.counter_id',
                    'token_setting.user_id'
                    )
                ->join('department', 'department.id', '=', 'token_setting.department_id')
                ->join('counter', 'counter.id', '=', 'token_setting.counter_id')
                ->join('user', 'user.id', '=', 'token_setting.user_id')
                ->where('token_setting.status', 1)
                ->groupBy('token_setting.department_id')
                ->get(); 
        }

        return $tokens;
    }    

    public function tokenGenerate(Request $request)
    {   
        @date_default_timezone_set(session('app.timezone'));  
        $display = DisplaySetting::first();
        $validator = Validator::make($request->all(), [
            'department_id' => 'required|max:11',
            'counter_id'    => 'required|max:11',
            'user_id'       => 'required|max:11',
            'note'          => 'max:512'
        ])
        ->setAttributeNames(array( 
           'department_id' => trans('app.department'),
           'counter_id' => trans('app.counter'),
           'user_id' => trans('app.officer'), 
           'note' => trans('app.note')
        ));  

        $manager = $this->tokenManager();
        if ($manager->success)
        {
            $data['status'] = false;
            $data['exception'] = trans('app.no_success_token_found_in_last_10_tokens');
            return response()->json($data); 
        }
        else if ($manager->pending)
        {
            $data['status'] = false;
            $data['exception'] = trans('app.wait_until_procced_the_pending_token');
            return response()->json($data); 
        }
        else if ($manager->stoped)
        {
            $data['status'] = false;
            $data['exception'] = trans('app.you_have_been_blocked_for_the_day');
            return response()->json($data); 
        }

        //generate a token
        try 
        {
            DB::beginTransaction(); 

            if ($validator->fails()) 
            {
                $data['status'] = false;
                $data['exception'] = "<ul class='list-unstyled'>"; 
                $messages = $validator->messages();
                foreach ($messages->all('<li>:message</li>') as $message)
                {
                    $data['exception'] .= $message; 
                }
                $data['exception'] .= "</ul>"; 
            } 
            else 
            {   
                //find auto-setting
                $settings = TokenSetting::select('counter_id','department_id','user_id','created_at')
                        ->where('department_id', $request->department_id)
                        ->groupBy('user_id')
                        ->get();

                //if auto-setting are available
                if (!empty($settings) && $display->display != 5) {

                    foreach ($settings as $setting) 
                    {
                        //compare each user in today
                        $tokenData = Token::select('department_id','counter_id','user_id',DB::raw('COUNT(user_id) AS total_tokens'))
                                ->where('department_id',$setting->department_id)
                                ->where('counter_id',$setting->counter_id)
                                ->where('user_id',$setting->user_id)
                                ->where('status', 0)
                                ->whereRaw('DATE(created_at) = CURDATE()')
                                ->orderBy('total_tokens', 'asc')
                                ->groupBy('user_id')
                                ->first(); 

                        //create user counter list
                        $tokenAssignTo[] = [
                            'total_tokens'  => (!empty($tokenData->total_tokens)?$tokenData->total_tokens:0),
                            'department_id' => $setting->department_id,
                            'counter_id'    => $setting->counter_id,
                            'user_id'       => $setting->user_id
                        ]; 
                    }

                    //findout min counter set to 
                    $min = min($tokenAssignTo);
                    $saveToken = [
                        'token_no'      => (new Token_lib)->newToken($min['department_id'], $min['counter_id']),
                        'client_mobile' => auth()->user()->mobile,
                        'department_id' => $min['department_id'],
                        'counter_id'    => $min['counter_id'],
                        'user_id'       => $min['user_id'],
                        'note'          => $request->note, 
                        'created_by'    => auth()->user()->id,
                        'created_at'    => date('Y-m-d H:i:s'), 
                        'updated_at'    => null,
                        'status'        => 0 
                    ]; 

                } 
                else 
                {
                    $saveToken = [
                        'token_no'      => (new Token_lib)->newToken($request->department_id, $request->counter_id),
                        'client_mobile' => auth()->user()->mobile,
                        'department_id' => $request->department_id,
                        'counter_id'    => $request->counter_id, 
                        'user_id'       => $request->user_id, 
                        'note'          => $request->note, 
                        'created_at'    => date('Y-m-d H:i:s'),
                        'created_by'    => auth()->user()->id,
                        'updated_at'    => null,
                        'status'        => 0
                    ];               
                }  

                //store in database  
                //set message and redirect
                $insert_id = Token::insertGetId($saveToken);
                if ($insert_id) { 

                    $token = null;
                    //retrive token info
                    $token = Token::select(
                            'token.*', 
                            'department.name as department', 
                            'counter.name as counter', 
                            'user.firstname', 
                            'user.lastname'
                        )
                        ->leftJoin('department', 'token.department_id', '=', 'department.id')
                        ->leftJoin('counter', 'token.counter_id', '=', 'counter.id')
                        ->leftJoin('user', 'token.user_id', '=', 'user.id') 
                        ->where('token.id', $insert_id)
                        ->first(); 

                    DB::commit();
                    $data['status'] = true;
                    $data['message'] = trans('app.token_generate_successfully');
                    $data['token']  = $token;
                    
                } 
                else 
                {
                    $data['status'] = false;
                    $data['exception'] = trans('app.please_try_again');
                }
            }
            
            return response()->json($data);
            
        } catch(\Exception $err) {
            DB::rollBack(); 
        }
    } 

	public function tokenReport()
	{
        $app  = Setting::first();
        date_default_timezone_set($app->timezone);
        $counters    = Counter::where('status',1)->pluck('name','id');
        $departments = Department::where('status',1)->pluck('name','id');
        $officers    = User::select(DB::raw('CONCAT(firstname, " ", lastname) as name'), 'id')
            ->where('user_type',1)
            ->where('status',1)
            ->orderBy('firstname', 'ASC')
            ->pluck('name', 'id');  
 
		return view('frontend.client.report', compact(
            'app',
            'counters',
            'departments',
            'officers'
        ));  
	}

    public function tokenData(Request $request)
    {
        $columns = [
            0 => 'id',
            1 => 'token_no',
            2 => 'status', 
            3 => 'department_id',
            4 => 'counter_id',
            5 => 'user_id', 
            6 => 'created_at',  
            7 => 'id',
        ]; 

        $totalData = Token::where('created_by', auth()->user()->id)->count();
        $totalFiltered = $totalData; 
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir   = $request->input('order.0.dir'); 
        $search = $request->input('search'); 
            
        if(empty($search))
        {            
            $tokens = Token::where('created_by', auth()->user()->id)
                 ->offset($start)
                 ->limit($limit)
                 ->orderBy($order,$dir)
                 ->get();
        }
        else 
        { 
            $tokensProccess = Token::where('created_by', auth()->user()->id)
                ->where(function($query)  use($search) {

                    if (!empty($search['status'])) {
                        $query->where('status', '=', $search['status']);
                    }
                    if (!empty($search['counter'])) {
                        $query->where('counter_id', '=', $search['counter']);
                    }
                    if (!empty($search['department'])) {
                        $query->where('department_id', '=', $search['department']);
                    }
                    if (!empty($search['officer'])) {
                        $query->where('user_id', '=', $search['officer']);
                    }

                    if (!empty($search['start_date']) && !empty($search['end_date'])) {
                        $query->whereBetween("created_at",[
                            date('Y-m-d', strtotime($search['start_date']))." 00:00:00", 
                            date('Y-m-d', strtotime($search['end_date']))." 23:59:59"
                        ]);
                    }
 
                    if (!empty($search['value'])) {

                        if ((strtolower($search['value']))=='vip') 
                        {
                            $query->where('is_vip', '1');
                        }
                        else
                        {
                            $date = date('Y-m-d', strtotime($search['value']));
                            $query->where('token_no', 'LIKE',"%{$search['value']}%")
                                ->orWhere('client_mobile', 'LIKE',"%{$search['value']}%")
                                ->orWhere('note', 'LIKE',"%{$search['value']}%")
                                ->orWhere(function($query)  use($date) {
                                    $query->whereDate('created_at', 'LIKE',"%{$date}%");
                                })
                                ->orWhere(function($query)  use($date) {
                                    $query->whereDate('updated_at', 'LIKE',"%{$date}%");
                                })
                                ->orWhereHas('generated_by', function($query) use($search) {
                                    $query->where(DB::raw('CONCAT(firstname, " ", lastname)'), 'LIKE',"%{$search['value']}%");
                                }); 
                        }
                    }
                });

            $totalFiltered = $tokensProccess->count();
            $tokens = $tokensProccess->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get(); 

        }

        $data = array();
        if(!empty($tokens))
        {
            $loop = 1;
            foreach ($tokens as $token)
            {  
                # complete time calculation
                $complete_time = "";
                if (!empty($token->updated_at)) {  
                    $date1 = new \DateTime($token->created_at); 
                    $date2 = new \DateTime($token->updated_at); 
                    $diff  = $date2->diff($date1); 
                    $complete_time = (($diff->d > 0) ? " $diff->d Days " : null) . "$diff->h Hours $diff->i Minutes ";
                }

                # options
                $options = "<div class='btn-group' style='width:70px'>";
                $options .= "<button type=\"button\" href=\"".route("client.token.print")."\" data-token-id='$token->id' class=\"tokenPrint btn btn-info btn-sm\" title=\"Print\"><i class=\"fa fa-eye\"></i></button>";
                if ($token->status == 0) {
                    $options .= "<a href=\"". url("client/token/stoped/$token->id")."\"  class=\"btn btn-danger btn-sm\" onclick=\"return confirm('Are you sure?')\" title=\"Stoped\"><i class=\"fa fa-stop\"></i></a>";
                } 
                $options .= "</div>"; 

                // details
                $details = "<ul class='list-unstyled p-0 m-0' style='min-width:200px'>";
                $details .= "<li><strong> ".trans('app.note')."</strong>". $token->note."</li>";
                $details .= "<li><strong>".trans('app.created_by')."</strong> ".(!empty($token->generated_by)?($token->generated_by->firstname." ". $token->generated_by->lastname):null)."</li>";
                $details .= "<li><strong>".trans('app.created_at')."</strong> ".(!empty($token->created_at)?date('j M Y h:i a',strtotime($token->created_at)):null)."</li>";
                $details .= "<li><strong>".trans('app.complete_time')."</strong> $complete_time</li>";
                $details .= "</ul>";

                $data[] = [
                    'serial'     => $loop++,
                    'token_no'   => (!empty($token->is_vip)?("<span class=\"label label-danger\" title=\"VIP\">$token->token_no</span>"):$token->token_no),
                    'status'     => (($token->status==1)?("<span class='label label-success'>".trans('app.complete')."</span>"):(($token->status==2)?"<span class='label label-danger'>".trans('app.stop')."</span>":"<span class='label label-primary'>".trans('app.pending')."</span>")).' '.(!empty($token->is_vip)?('<span class="label label-info" title="VIP">VIP</span>'):''),
                    'department' => (!empty($token->department)?$token->department->name:null),
                    'counter'    => (!empty($token->counter)?$token->counter->name:null),
                    'officer'    => (!empty($token->officer)?$token->officer->firstname:null)." ". (!empty($token->officer)?$token->officer->lastname:null),  
                    'details'    => $details,
                    'options'    => $options
                ];  
            }
        }
            
        return response()->json([
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        ]);
    } 

    public function tokenShow(Request $request)
    {
        return Token::select('token.*', 'department.name as department', 'counter.name as counter', 'user.firstname', 'user.lastname')
            ->leftJoin('department', 'token.department_id', '=', 'department.id')
            ->leftJoin('counter', 'token.counter_id', '=', 'counter.id')
            ->leftJoin('user', 'token.user_id', '=', 'user.id')
            ->where('token.id', $request->id)
            ->where('created_by', auth()->user()->id)
            ->first(); 
    }

    public function stoped($id)
    {  
        Token::where('id', $id)
        ->where('created_by', auth()->user()->id)
        ->where('status', '0')
        ->update([
            'updated_at' => null, 
            'status' => 2,
            'sms_status' => 1
        ]);
        return redirect()->back()->with('message', trans('app.update_successfully'));
    } 

	# -----------------------------------------------------------
	# DASHBOARD 
	# ----------------------------------------------------------- 

    public function dashboard()
    {
        $app  = Setting::first();
        date_default_timezone_set($app->timezone);
        
        $charts = $this->charts(); 

        return view('frontend.client.dashboard', compact(
            'app',
            'charts' 
        ));  
    }


    private function charts()
    {  
        $month = DB::select(DB::raw("
            SELECT 
                DATE_FORMAT(created_at, '%d') AS date,
                COUNT(CASE WHEN status = 1 THEN 1 END) as success,
                COUNT(CASE WHEN status = 0 THEN 1 END) as pending,
                COUNT(t.id) AS total
            FROM 
                token AS t
            WHERE 
                DATE(created_at) >= DATE_SUB(CURDATE(),INTERVAL 1 MONTH)
                AND  
                t.created_by = '". auth()->user()->id  ."' 
            GROUP BY 
                DATE(created_at)
            ORDER BY 
                t.id ASC
        "));

        $year = DB::select(DB::raw("
            SELECT 
                DATE_FORMAT(created_at, '%M') AS month,
                COUNT(CASE WHEN status = 1 THEN 1 END) as success,
                COUNT(CASE WHEN status = 0 THEN 1 END) as pending,
                COUNT(t.id) AS total
            FROM 
                token AS t
            WHERE 
                DATE(created_at) >= DATE_SUB(CURDATE(),INTERVAL 1 YEAR)
                AND 
                t.created_by = '". auth()->user()->id  ."' 
            GROUP BY 
                month
            ORDER BY 
                t.created_at ASC
        "));

        $begin = DB::select(DB::raw("
            SELECT 
                DATE(created_at) AS date,
                COUNT(CASE WHEN status = 1 THEN 1 END) as success,
                COUNT(CASE WHEN status = 0 THEN 1 END) as pending,
                COUNT(t.id) AS total
            FROM 
                token AS t   
            WHERE 
                t.created_by = '". auth()->user()->id  ."' 
        ")); 
 

        // created by me
        $token = Token::where('created_by', '=', auth()->user()->id)
            ->selectRaw("COUNT(id) as total, status")
            ->groupBy('status')
            ->pluck("total", "status");
 

        return (object)[
        	'month' => $month,
        	'year'  => $year,
        	'begin' => $begin,
        	'token' => $token
        ];
    } 

}

