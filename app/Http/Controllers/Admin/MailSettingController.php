<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Common\MailController AS MailController;
use App\Models\MailSetting;
use App\Models\MailHistory;
use App\Models\User;
use App\Models\Setting; 
use Validator;

class MailSettingController extends Controller
{ 
    # Show the email list
    public function show()
    { 
        $emails = MailHistory::all();
        return view('backend.admin.mail.list', compact('emails'));
    }
  
    public function mailData(Request $request) 
    {
        $columns = [
            0 => 'id',
            1 => 'email',
            2 => 'subject',
            3 => 'message',
            4 => 'created_at',
            5 => 'status',
            6 => 'id' 
        ]; 
  
        $totalData = MailHistory::count();
        $totalFiltered = $totalData; 
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir   = $request->input('order.0.dir'); 
        $search = $request->input('search'); 
            
        if(empty($search))
        {            
            $mails = MailHistory::offset($start)
                 ->limit($limit)
                 ->orderBy($order,$dir)
                 ->get();
        }
        else 
        { 
            $mailsProccess = MailHistory::where(function($query)  use($search) {

                if (!empty($search['start_date']) && !empty($search['end_date'])) {
                    $query->whereBetween("created_at",[
                        date('Y-m-d', strtotime($search['start_date']))." 00:00:00", 
                        date('Y-m-d', strtotime($search['end_date']))." 23:59:59"
                    ]);
                }
         
                if (!empty($search['value'])) {
                    $query->orWhere('email', 'LIKE',"%{$search['value']}%")
                        ->orWhere('subject', 'LIKE',"%{$search['value']}%") 
                        ->orWhere('message', 'LIKE',"%{$search['value']}%") 
                        ->orWhere(function($query)  use($search) {
                            $date = date('Y-m-d', strtotime($search['value']));
                            $query->whereDate('created_at', 'LIKE',"%{$date}%");
                        }); 
                }
            });
        
            $totalFiltered = $mailsProccess->count();

            $query = $mailsProccess;
            if ($limit != -1) {
                $query->offset($start)
                    ->limit($limit);
            }
            $query->orderBy($order,$dir);
            $mails = $query->get(); 
        }
 
        $data = array();
        if(!empty($mails))
        {
            $loop = 1;
            foreach ($mails as $mail)
            $data[] = [
                'serial'     => $loop++,
                'email'      => $mail->email,
                'subject'    => $mail->subject,
                'message'    => $mail->message, 
                'created_at' => (!empty($mail->created_at)?date('j M Y h:i a',strtotime($mail->created_at)):null),
                'status'     => (($mail->status==1)?"<span class='label label-success'>".trans('app.complete')."</span>":"<span class='label label-warning'>".trans('app.pending')."</span>"),
                'options'    => "<div class=\"btn-group\">
                  <a href='".url("admin/mail/delete/$mail->id")."' onclick=\"return confirm('".trans('app.are_you_sure')."')\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-times\"></i></a>
                </div>" 
            ];   
        }
            
        return response()->json([
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        ]);
    }

    # Email form
    public function form()
    {  
    	$setting = MailSetting::first();
        return view('backend.admin.mail.form', compact('setting'));
    }

    # Send a mail
    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'mail_setting_id' => 'required|max:11',
            'email'       => 'required|max:100',
            'subject'     => 'required|max:255',
            'message'     => 'max:6000',
        ]);  

        if ($validator->fails()) 
        {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        } 
        else 
        {   
            $mail = (new MailController)->send([ 
                'email'   => $request->email,
                'subject' => $request->subject,
                'message' => $request->message
            ]);
  
            if ($mail) 
            { 
                return back()
                    ->with('message', trans('app.mail_sent'))
                    ->withInput();
            } 
            else 
            { 
                return back()
                    ->withInput()
                    ->with('exception', trans('app.please_try_again'))
                    ->withErrors($validator);
            }
        } 
    }
   
    # Show the email setting form
    public function setting(Request $request)
    {
        $setting = MailSetting::first(); 
        if (!$setting) 
        {
            $data = new MailSetting;
            $data->driver     = 'smtp';
            $data->host       = 'mailtrap.io';
            $data->port       = 2525;
            $data->username   = '89955d8d34c48e';
            $data->password   = '097ce508ab4744';
            $data->encryption = 'tls';
            $data->sendmail   = 'usr/sbin/sendmail -bs';
            $data->pretend    = false;
            $data->save();
        }  

        return view('backend.admin.mail.setting', compact('setting'));
    }
 

    # mail setting
    public function updateSetting(Request $request)
    {  
        $validator = Validator::make($request->all(), [ 
            'driver'     => 'required|max:64',
            'host'       => 'required|max:64',
            'port'       => 'required|max:50',
            'username'   => 'required|max:64', 
            'password'   => 'required|max:64', 
            'encryption' => 'required|max:50', 
            'sendmail'   => 'required|max:128', 
            'pretend'    => 'required|max:10', 
        ]); 

        if ($validator->fails()) 
        {
            return back()
                ->with('error', trans('app.please_try_again'))
                ->withErrors($validator)
                ->withInput();
        } 
        else 
        {  
            $config = array(
                'driver'     => $request->driver,
                'host'       => $request->host,
                'port'       => $request->port, 
                'username'   => $request->username,
                'password'   => $request->password,
                'encryption' => $request->encryption, 
                'sendmail'   => (!empty($request->sendmail)?$request->sendmail:'/usr/sbin/sendmail -bs'),
                'pretend'    => (!empty($request->pretend)?true:false),
            );

            $update = MailSetting::where("id", $request->id)->update($config); 

            if ($update) 
            { 
                cache()->forget('mail');
                
                return back()
                    ->with('message', trans('app.update_successfully'))
                    ->withInput();
            } 
            else 
            {
                return back()
                    ->withInput()
                    ->with('exception', trans('app.please_try_again'))
                    ->withErrors($validator);
            }
        }
    }


    # Delete mail history data by id_no
    public function delete(Request $request)
    {
        // request by id_no
        $history = MailHistory::where('id', $request->id)->delete();

        if ($history)
        { 
            return back()->with('message', trans('app.delete_successfully'));
        } else { 
            return back()->with('exception', trans('app.please_try_again'));
        }
    } 

    # all email list
    public function contract(Request $request)
    {
        return User::distinct("email")
        ->whereNotNull('email')
        ->where('email', 'like', "%$request->email%")
        ->orderBy('email', 'asc')
        ->pluck('email');  
    } 
}
