<?php
namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\MailHistory;
use App\Mail\Template;
use Mail;

class MailController extends Controller
{
	public function send($data = [])
	{
		Mail::to($data['email'])
		    ->send(new Template($data['subject'], $data['message']));

		//store email information 
		$email = new MailHistory; 
		$email->email       = $data['email'];
		$email->subject     = $data['subject'];
		$email->message     = $data['message'];
		$email->created_at  = date('Y-m-d H:i:s');
		$email->updated_at  = null;
		$email->created_by  = (!empty(auth()->user()->id)?auth()->user()->id:null); 

		if( count(Mail::failures()) > 0 )
		{
		    $email->status  = 0;
		    $email->save();

		    return response()->json([
		    	'status'  => false,
		    	'message' => trans('app.please_try_again'),
		    	'data'    => ''
		    ]);
		} 
		else
		{ 
			$email->status      = 1; //sent
		    $email->save();

		    return response()->json([
		    	'status'  => true,
		    	'message' => trans('app.mail_sent'),
		    	'data'    => ''
		    ]);
		}
	}


	public function retry($data = [])
	{ 
		Mail::to($data['email'])
		    ->send(new Template($data['subject'], $data['message']));

		//store email information 
        $email = MailHistory::find($data['id']);
        $email->updated_at  = date('Y-m-d H:i:s'); 

		if( count(Mail::failures()) > 0 )
		{
		    $email->status  = 0;
		    $email->save();

		    return response()->json([
		    	'status'  => false,
		    	'message' => trans('app.please_try_again'),
		    	'data'    => ''
		    ]);
		} 
		else
		{ 
			$email->status = 1; //sent
		    $email->save();

		    return response()->json([
		    	'status'  => true,
		    	'message' => trans('app.mail_sent'),
		    	'data'    => ''
		    ]);
		}
	}

}