<?php
namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Models\User;
use App\Models\Setting;
use App\Models\DisplaySetting;
use App\Models\DisplayCustom;
use App\Models\Token; 
use Auth, Session, DB, Hash;

class LoginController extends Controller
{
    use ThrottlesLogins; 
    
    public function login()
    {      
        $customDisplays = DisplayCustom::where('status', 1)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id');
        if (!empty($customDisplays))
        {
            \Session::put('custom_displays', $customDisplays); 
        }

        $app     = Setting::first();
        if(!empty($app)) 
        {
            \Session::put('app', array(
                'title'    => $app->title, 
                'favicon'  => $app->favicon, 
                'logo'     => $app->logo, 
                'timezone' => $app->timezone,
                'language' => $app->language, 
                'website'  => $app->website, 
                'copyright_text' => $app->copyright_text, 
            )); 
        } 

        $ds = DisplaySetting::first(); 
        \Session::put('display', !empty($ds->display)?$ds->display:2);
        
        return view('layouts.login');
    }


    public function checkLogin(Request $request)
    {
        //start login throttoling
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return redirect('auth/login')->with('exception', trans('app.to_many_login_attempts'));
        }
        $this->incrementLoginAttempts($request);
        //end login throttoling

        if (\Auth::attempt(['email'=>$request->email, 'password'=>$request->password]))
        {

            $authUser = Auth::user();
            unset($authUser->password);  

            if ($authUser->status == '0') 
            { 
                return redirect('auth/login')->with('exception', trans('app.your_account_is_not_confirm_yet'));
            } 
            else if (in_array($authUser->user_type, [1,2,5])) 
            {
                Auth::login($authUser, true); 
                return redirect(strtolower(auth()->user()->role()));
            } 
            else 
            { 
                return redirect('auth/login')->with('exception', trans('app.contact_with_authurity'));
            }

        }
        else
        {
            return redirect()->back()->with('exception', trans('app.invalid_credential'));
        }
    }  

    public function logout()
    { 
        Session::flush();
        Auth::logout();        
        return redirect('auth/login')->with('message', trans('app.signout_successfully'));
    }

    //prerequisite for login throttling
    public function username()
    {
        return 'email';
    } 
}
