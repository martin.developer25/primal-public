@extends('layouts.display')
@section('title', trans('app.display_1'))


@section('content')
  <div class="panel-titulo">
    <div class="div-titulo">
        <span class="titulo-header">PLANSUAREZ</span>
    </div>
  </div>
    <div id="fullscreen">
      <div class="row" style="background: white">  
         <div id="display1"></div>
      </div>
 
      <div class="panel-footer col-xs-12"> 
        @include('backend.common.info')
        <span class="col-xs-10 text-left">@yield('info.powered-by')</span>
        <span class="col-xs-2 text-right">@yield('info.version')</span>
      </div>
    </div> 
@endsection

@push('scripts')
<script type="text/javascript"> 
$(document).ready(function(){
  //get previous token
  var view_token = [];
  var interval = 1000; 

  var display = function()
  {
    var width  = $(window).width();
    var height = $(window).height();
    var isFullScreen = document.fullScreen ||
    document.mozFullScreen ||
    document.webkitIsFullScreen || (document.msFullscreenElement != null);
    if (isFullScreen)
    {
      var width  = $(window).width();
      var height = $(window).height();
    } 
    
    $.ajax({
        type:'post',
        url:'{{ URL::to("common/display1") }}',
        data:
        {
            _token: '<?php echo csrf_token() ?>',
            view_token: view_token,
            width: width,
            height: height
        },
       success:function(data)
       {
          $("#display1").html(data.result);
 
          view_token = data.view_token;

          //notification sound
          if (data.status)
          {  
              var url  = "{{ URL::to('') }}"; 
              var lang = "{{ in_array(session()->get('locale'), $setting->languages)?session()->get('locale'):'en' }}";
              var player = new Notification;
              player.call([data.new_token], lang, url);
          } 

          // setTimeout(display, data.interval);
       }
    });
  };

  setTimeout(display, interval);

});
</script>
@endpush

