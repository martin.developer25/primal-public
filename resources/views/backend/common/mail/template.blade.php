<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{{ $app->title }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                            <a href="{{ url('/') }}">
                                <img src="{{ asset((!empty($app->logo)?$app->logo:'public/assets/img/icons/logo.png')) }}" alt="{{ ((!empty($app->title)?$app->title:'Logo')) }}" style="width:88%;max-height:200px;display: block;" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                                <tr>
                                    <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                        {!! !empty($app->message)?$app->message:null !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="260" valign="top" style=" color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
													<h4 style="margin:0;"><a href="{{ url('/') }}">{{ $app->title }}</a></h4>
													<p style="padding:0;margin:5px 0;">{!! $app->description !!}</p>
													<h4 style="margin:0">{{ trans('app.address') }}</h4>
													<p style="padding:0;margin:5px 0;">{{ $app->address }}</p>
													<h4 style="margin:0">{{ trans('app.email') }}</h4>
													<p style="padding:0;margin:5px 0;">{{ $app->email }}</p> 
													<h4 style="margin:0">{{ trans('app.mobile') }}</h4>
													<p style="padding:0;margin:5px 0;">{{ $app->phone }}</p> 
                                                </td> 
                                                <td style="font-size: 0; line-height: 0;" width="20">
                                                    &nbsp;
                                                </td>
                                                <td width="260" valign="top">
						                            @if ($app->google_map)
						                                {!! $app->google_map !!}
						                            @else
						                                <img src="{{ asset('public/assets/img/icons/map.PNG') }}" alt="Google Embeded Map"/>
						                            @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td  style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; text-align: center;" width="100%" colspan="2">
                                        {{ (!empty($app->copyright_text)?$app->copyright_text:'CodeKernel.Net') }} 
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
