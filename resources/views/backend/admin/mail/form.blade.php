@extends('layouts.backend')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="text-left">{{ trans('app.new_mail') }}</h3>
    </div>

    <div class="panel-body">
        {!! Form::open(['url' => 'admin/mail/new', 'class' => 'form-validation frmValidation', 'files' => true]) !!}

        {{ Form::hidden('mail_setting_id', (!empty($setting->id)?$setting->id:1)) }}
        <div class="row">
            <div class="col-sm-8"> 
                <label for="email">{{ trans('app.email') }}</label>
                <div class="form-group">
                    <div class="form-line  {{ $errors->has('email') ? 'error focused' : '' }}">
                        <input name="email" type="text" id="email" class="form-control" placeholder="{{ trans('app.email') }}" value="{{ old('email') }}" autocomplete="off">
                    </div> 
                    <span class="text-danger">{{ $errors->first('email') }}</span> 
                </div>

                <label for="subject">{{ trans('app.subject') }}</label>
                <div class="form-group">
                    <div class="form-line  {{ $errors->has('subject') ? 'error focused' : '' }}">
                        <input name="subject" type="text" id="subject" class="form-control" placeholder="{{ trans('app.subject') }}" value="{{ old('subject') }}">
                    </div>
                    <span class="text-danger">{{ $errors->first('subject') }}</span> 
                </div>

                <label for="message">{{ trans('app.message') }}</label>
                <div class="form-group">
                    <div class="form-line  {{ $errors->has('message') ? 'error focused' : '' }}">
                        <textarea name="message" type="text" id="message" class="form-control" rows="6" placeholder="{{ trans('app.message') }}">{{ old('message') }}</textarea>
                    </div>
                    <span class="text-danger">{{ $errors->first('message') }}</span> 
                </div>

                <div class="form-group">
                    <button type="reset" class="btn btn-info waves-effect">{{ trans('app.reset') }}</button>
                    <button type="submit" class="btn btn-success waves-effect">{{ trans('app.send') }}</button>
                </div>
            </div> 
        </div>
        {!! Form::close() !!}
    </div> 
</div>
@endsection

@push('scripts')
<script> 
$(document).ready(function(){  
    $("#email").autocomplete({
        source: function( request, response ) {  
            $.ajax({
                url: "{{ url('admin/mail/contract') }}",
                dataType: "json",
                data: {email: request.term},
                success: function( data ) { 
                    response( data );
                },
                select: function (event, ui) {
                    return false;
                }
            });
        } 
    });  
})
</script>
@endpush