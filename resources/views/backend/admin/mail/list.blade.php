@extends('layouts.backend')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="text-left">{{ trans('app.mail_history') }}</h3>
    </div>

    <div class="panel-body"> 
        <table class="dataTables-server display table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">#</th>
                    <td>
                        <label>{{ trans('app.start_date') }}</label><br/>
                        <input type="text" class="datepicker form-control input-sm filter" id="start_date" placeholder="{{ trans('app.start_date') }}" autocomplete="off" style="width:100px" />
                    </td>
                    <td>
                        <label>{{ trans('app.end_date') }}</label><br/>
                        <input type="text" class="datepicker form-control input-sm filter" id="end_date" placeholder="{{ trans('app.end_date') }}" autocomplete="off" style="width:100px"/>
                    </td>
                    <th colspan="4"></th>
                </tr> 
                <tr>  
                    <th>{{ trans('app.email') }}</th>
                    <th>{{ trans('app.subject') }}</th>
                    <th>{{ trans('app.message') }}</th>
                    <th>{{ trans('app.date') }}</th>
                    <th>{{ trans('app.status') }}</th>
                    <th>{{ trans('app.action') }}</th> 
                </tr>
            </thead>  
        </table>
    </div>
</div>
<!-- #END# Exportable Table -->
@endsection

 
@push('scripts') 
<script> 
$(document).ready(function(){
    // DATATABLE
    drawDataTable();

    $("body").on("change",".filter", function(){
        drawDataTable();
    });

    function drawDataTable()
    {  
        $('.dataTables-server').DataTable().destroy();
        $('.dataTables-server').DataTable({
            responsive: true, 
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('admin/mail/data') }}",
                dataType: 'json',
                type    : 'post',
                data    : {
                    _token : '{{ csrf_token() }}', 
                    search: {
                        start_date : $('#start_date').val(),
                        end_date   : $('#end_date').val(),
                    }
                }
            },
            columns: [ 
                {data: 'serial' },
                {data: 'email'},
                {data: 'subject'},
                {data: 'message'},
                {data: 'created_at'},
                {data: 'status'}, 
                {data: 'options', orderable: false, searchable: false} 
            ],  
            order: [ [0, 'desc'] ], 
            select    : true,
            pagingType: "full_numbers",
            lengthMenu: [[25, 50, 100, 150, 200, 500, -1], [25, 50, 100, 150, 200, 500, "All"]],
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>><'row'<'col-sm-12't>><'row'<'col-sm-6'i><'col-sm-6'p>>", 
            buttons: [
                { extend:'copy', text:'<i class="fa fa-copy"></i>', className:'btn-sm',exportOptions:{columns:':visible'}},
                { extend: 'print', text  :'<i class="fa fa-print"></i>', className:'btn-sm', exportOptions: { columns: ':visible',  modifier: { selected: null } }},  
                { extend: 'print',className:'btn-sm', text:'<i class="fa fa-print"></i>  Selected',exportOptions:{columns: ':visible'}},  
                { extend:'excel',  text:'<i class="fa fa-file-excel-o"></i>', className:'btn-sm',exportOptions:{columns:':visible'}},
                { extend:'pdf',  text:'<i class="fa fa-file-pdf-o"></i>',  className:'btn-sm',exportOptions:{columns:':visible'}},
                { extend:'colvis', text:'<i class="fa fa-eye"></i>',className:'btn-sm'} 
            ] 
        });
    } 

}); 
</script>
@endpush