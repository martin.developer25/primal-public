@extends('layouts.backend')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="text-left">{{ trans('app.mail_setting') }}</h3>
    </div>

    <div class="panel-body">
        {!! Form::open(['url' => 'admin/mail/setting', 'class' => 'form-validation col-sm-7 col-md-6']) !!}

            {!! Form::hidden('id', $setting->id) !!}
            
            <label for="driver">{{ trans('app.driver') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('driver') ? 'error focused' : '' }}">
                    <input name="driver" type="text" id="driver" class="form-control" placeholder="{{ trans('app.driver') }}" value="{{ $setting->driver }}">
                </div>
                <span class="text-danger">{{ $errors->first('driver') }}</span>
            </div>

            <label for="host">{{ trans('app.host') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('host') ? 'error focused' : '' }}">
                    <input name="host" type="text" id="host" class="form-control" placeholder="{{ trans('app.host') }}" value="{{ $setting->host }}">
                </div>
                <span class="text-danger">{{ $errors->first('host') }}</span>
            </div>

            <label for="port">{{ trans('app.port') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('port') ? 'error focused' : '' }}">
                    <input name="port" type="text" id="port" class="form-control" placeholder="{{ trans('app.port') }}" value="{{ $setting->port }}">
                </div>
                <span class="text-danger">{{ $errors->first('port') }}</span> 
            </div>

            <label for="username">{{ trans('app.username') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('username') ? 'error focused' : '' }}">
                    <input name="username" type="text" id="username" class="form-control" placeholder="{{ trans('app.username') }}" value="{{ $setting->username }}">
                </div>
                <span class="text-danger">{{ $errors->first('username') }}</span> 
            </div>

            <label for="password">{{ trans('app.password') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('password') ? 'error focused' : '' }}">
                    <input name="password" type="password" id="password" class="form-control" placeholder="{{ trans('app.password') }}" value="{{ $setting->password }}">
                </div>
                <span class="text-danger">{{ $errors->first('password') }}</span> 
            </div>

            <label for="encryption">{{ trans('app.encryption') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('encryption') ? 'error focused' : '' }}">
                    <input name="encryption" type="text" id="encryption" class="form-control" placeholder="{{ trans('app.encryption') }}" value="{{ $setting->encryption }}">
                </div>
                <span class="text-danger">{{ $errors->first('encryption') }}</span>  
            </div>

            <label for="sendmail">{{ trans('app.sendmail') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('sendmail') ? 'error focused' : '' }}">
                    <input name="sendmail" type="text" id="sendmail" class="form-control" placeholder="{{ trans('app.sendmail') }}" value="{{ $setting->sendmail }}">
                </div>
                <span class="text-danger">{{ $errors->first('sendmail') }}</span>
            </div>

            <label for="pretend">{{ trans('Pretend') }}</label>
            <div class="form-group">
                <div class="form-line  {{ $errors->has('pretend') ? 'error focused' : '' }}">
                    <input name="pretend" type="text" id="pretend" class="form-control" placeholder="{{ trans('Pretend') }}" value="{{ $setting->pretend }}">
                </div>
                <span class="text-danger">{{ $errors->first('pretend') }}</span>
            </div>

            <button type="reset" class="btn btn-warning waves-effect">{{ trans('Reset') }}</button>
            <button type="submit" class="btn btn-success waves-effect">{{ trans('Update') }}</button>
        {!! Form::close() !!}

        <div class="col-md-6 col-sm-5">
            <div class="panel-heading"><span class="label label-danger">EXAMPLE</span> GMAIL SMTP CONFIGURATION</div> 
            <div class="well">
                <p>Please configure the Gmail SMTP and then set the password to the email setting.</p>

                <p>An App Password is a 16-digit passcode that gives a non-Google app or device permission to access your Google Account. App Passwords can only be used with accounts that have 2-Step Verification turned on.</p> 
                 
                <p>Please follow the tutorial <a href="https://support.google.com/accounts/answer/185833?hl=en" target="_blank">https://support.google.com/accounts/answer/185833?hl=en</a> </p>
            </div> 
            <ul class="well list-unstyled">
                <li><strong class="label label-info">DRIVER</strong> smtp</li>
                <li><strong class="label label-info">HOST</strong> smtp.gmail.com</li>
                <li><strong class="label label-info">PORT</strong> 587</li>
                <li><strong class="label label-info">USERNAME</strong> Your full Gmail address</li>
                <li><strong class="label label-info">PASSWORD</strong> Your Gmail or Google Apps Password</li> 
                <li><strong class="label label-info">ENCRYPTION</strong> tls/ssl  </li>
                <li><strong class="label label-info">SEND MAIL</strong> usr/sbin/sendmail -bs </li>
                <li><strong class="label label-info">PRETEND</strong> 0 </li>
            </ul>    
        </div>
    </div>
</div> 
@endsection
