@extends('layouts.backend')
@section('title', trans('app.auto_token'))

@section('content')

<div class="panels-body">
    <div class="panel-titulo">
        <div class="div-titulo">
            <span class="titulo-header">PLANSUAREZ</span>
        </div>
    </div>

    <div class="panel-body-send">
        <div class="div-send">
            <img src="{{ asset('public/assets/img/ui/sent.png') }}" width="200px" height="200px;"/>
            <h1 class="text-send"> Se ha enviado un SMS con su turno</h1>
        </div>
    </div> 
</div>
@endsection

@push("scripts")
@endpush
 
 