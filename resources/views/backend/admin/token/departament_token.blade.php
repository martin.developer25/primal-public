@extends('layouts.backend')
@section('title', trans('app.auto_token'))

@section('content')

<div class="panel panel-primary panel-div" id="toggleScreenArea">
    <div class="panel-titulo">
        <div class="div-titulo">
            <span class="titulo-header">PLANSUAREZ</span>
        </div>
    </div>

    <div class="panels-body">
        @foreach ($departmentList as $department )
        @if($department->department_id == 1)
        <div class="div-departament-body" style="background: linear-gradient(315deg, #11122B, #6F64AA)">
            @else
            <div class="div-departament-body" style="background: linear-gradient(315deg, #60002D, #BC2E2E)">
                @endif

                {{ Form::open(['url' => 'admin/token/autoview', 'class' => 'form-departament']) }}
                <button type="submit" class="action-button">
                    <img src="{{ asset('public/assets/img/ui') }}/{{$department->img}}" width="260" height="280"><br>
                    <span class="departament-titulo">{{ strtoupper($department->name) }}</span>
                    <input type="text" name="department_id" value="{{$department->department_id}}" hidden>
                </button>
                {{ Form::close() }}
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection