@extends('layouts.backend')
@section('title', trans('app.auto_token'))

@section('content')

<div class="panels-body">
    <div class="panel-titulo">
        <div class="div-titulo">
            <span class="titulo-header">PLANSUAREZ</span>
        </div>
    </div>

    <div class="panel-body" style="background: linear-gradient(315deg, #11122B, #6F64AA); width: 100%; height: 100%;">
        <div class="div-image-phone">
            <img src="{{ asset('public/assets/img/ui/mobile.png') }}" width="200px" height="200px;" />
        </div>
        <h1 class="titulo-numero"> Ingrese su número de teléfono</h1>
        
        <div class="div-phone">
            {{ Form::open(['url' => 'admin/token/auto']) }}
                <select class="select-phone" name="operadora">
                    <option>0412</option>
                    <option>0414</option>
                    <option>0424</option>
                    <option>0416</option>
                    <option>0426</option>
                </select>
                <input type="text" name="telefono" class="phone-input" maxlength="7" minlength="7">
                <input type="text" name="department_id" value="{{$department_id}}" hidden>
                <input type="submit" value="" hidden>
            {{ Form::close() }}
    </div> 
</div>  

@endsection

@push("scripts")
<script type="text/javascript">

</script>
@endpush
 
 