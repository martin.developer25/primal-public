@extends('layouts.backend')
@section('title', trans('app.app_setting'))

@push('styles')
<link  href="{{ url('public/assets/css/summernote.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="panel panel-primary">

    <div class="panel-heading"> 
        <ul class="row list-inline m-0">
            <li class="col-xs-8 p-0 text-left">
                <h3>{{ trans('app.app_setting') }}</h3>
            </li>             
            <li class="col-xs-4 p-0 text-right"> 
                <h3>
                    @if (session()->get('app.website')) 
                        <a href="{{ url('/') }}" target="_blank" class="btn btn-success btn-sm">
                            <span class="fa fa-check"></span> {{ trans('app.website_active') }}
                        </a>
                    @else 
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm">
                            <span class="fa fa-times"></span> {{ trans('app.website_inactive') }}
                        </a>
                    @endif
                </h3>
            </li> 
        </ul>
    </div>

    <div class="panel">  
        <!-- Application -->
        <div class="col-sm-6 mt-1 pl-0">   
            {{ Form::open(['url' => 'admin/setting', 'files' => true, 'class'=>'panel panel-primary']) }}
            <div class="panel-heading">{{ trans('app.basic_setting') }}</div>
            <div class="panel-body">   
                <input type="hidden" name="id" value="{{ $setting->id }}">
         
                <div class="form-group @error('title') has-error @enderror">
                    <label for="title">{{ trans('app.title') }} <i class="text-danger">*</i></label> 
                    <input type="text" name="title" id="title" class="form-control" placeholder="{{ trans('app.title') }}" value="{{ old('title')?old(
                    'title'):$setting->title }}">
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                </div>

                <div class="form-group @error('description') has-error @enderror">
                    <label for="description">{{ trans('app.description') }} </label>
                    <textarea name="description" id="description" class="form-control" placeholder="{{ trans('app.description') }}">{{ old('description')?old(
                    'description'):$setting->description }}</textarea>
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>

                <div class="form-group @error('email') has-error @enderror">
                    <label for="email">{{ trans('app.email') }}</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="{{ trans('app.email') }}" value="{{ old('email')?old(
                    'email'):$setting->email }}">
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>

                <div class="form-group @error('phone') has-error @enderror">
                    <label for="phone">{{ trans('app.mobile') }}</label>
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="{{ trans('app.mobile') }}"  value="{{ old('phone')?old(
                    'phone'):$setting->phone }}">
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                </div>

                <div class="form-group @error('address') has-error @enderror">
                    <label for="address">{{ trans('app.address') }} </label>
                    <textarea name="address" id="address" class="form-control" placeholder="{{ trans('app.address') }}">{{ old('address')?old(
                    'address'):$setting->address }}</textarea>
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>

                <div class="form-group @error('copyright_text') has-error @enderror">
                    <label for="copyright_text">{{ trans('app.copyright') }} </label>
                    <textarea name="copyright_text" id="copyright_text" class="form-control" placeholder="{{ trans('app.copyright') }}">{{ old('copyright_text')?old(
                    'copyright_text'):$setting->copyright_text }}</textarea>
                    <span class="text-danger">{{ $errors->first('copyright_text') }}</span>
                </div>

                <div class="form-group @error('language') has-error @enderror">
                    @include('backend.common.info')
                    <label for="lang-select">{{ trans('app.language') }} </label>
                    @yield('info.language')
                    <span class="text-danger">{{ $errors->first('language') }}</span>
                </div> 

                <div class="form-group @error('timezone') has-error @enderror">
                    <label for="timezone">{{ trans('app.timezone') }} <i class="text-danger">*</i></label><br/>
                    {{ Form::select('timezone', $timezoneList, (old('timezone')?old(
                                    'timezone'):$setting->timezone) , [ 'class'=>'select2 form-control', "id"=>'timezone']) }}<br/>
                    <span class="text-danger">{{ $errors->first('timezone') }}</span>
                </div> 

                <div class="form-group @error('favicon') has-error @enderror">
                    <label for="favicon">{{ trans('app.favicon') }} </label>
                    <img src="{{ asset((session('favicon')?session('favicon'):$setting->favicon)) }}" alt="favicon" class="img-thubnail thumbnail" width="50" height="50"> 
                    <input type="hidden" name="old_favicon" value="{{ ((session('favicon') != null) ? session('favicon') : $setting->favicon) }}">  
                    <input type="file" name="favicon" id="favicon" class="form-control">
                    <span class="text-danger">{{ $errors->first('favicon') }}</span>
                    <span class="help-block">Diamension: (32x32)px</span>
                </div>

                <div class="form-group @error('logo') has-error @enderror">
                    <label for="wlogo">{{ trans('app.logo') }}</label>
                    <img src="{{ asset($setting->logo) }}" alt="" class="img-thubnail thumbnail" width="200"> 
                    <input type="hidden" name="old_logo" value="{{ $setting->logo }}"> 
                    <input type="file" name="logo" id="wlogo">
                    <span class="text-danger">{{ $errors->first('logo') }}</span>
                    <span class="help-block">Diamension: (250x50)px</span>
                </div>
                
                <div class="form-group text-right">
                    <button class="button btn btn-info" type="reset"><span>{{ trans('app.reset') }}</span></button>
                    <button class="button btn btn-success" type="submit"><span>{{ trans('app.update') }}</span></button> 
                </div>
            </div>
            {{ Form::close() }}
        </div>

        <!-- Website -->
        <div class="col-sm-6 mt-1 pr-0">   
            {{ Form::open(['url' => 'admin/setting/frontend', 'class'=>'panel panel-primary']) }}
            <div class="panel-heading">{{ trans('app.website_setting') }}</div>
            <div class="panel-body">
                <input type="hidden" name="id" value="{{ $setting->id }}">

                <div class="form-group @error('website') has-error @enderror">
                    <label for="website">{{ trans('app.website') }} <i class="text-danger">*</i></label>
                    <div id="website"> 
                        <label class="radio-inline">
                            <input type="radio" name="website" value="1" {{ ((old('website') || $setting->website)==1)?"checked":"" }}> {{ trans('app.active') }}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="website" value="0" {{ ((old('website') || $setting->website)==0)?"checked":"" }}> {{ trans('app.inactive') }}
                        </label> 
                    </div>
                </div>  

                <div class="form-group @error('meta_title') has-error @enderror">
                    <label for="meta_title">{{ trans('app.meta_title') }} </label>
                    <textarea name="meta_title" id="meta_title" class="form-control" placeholder="{{ trans('app.meta_title') }}">{{ old('meta_title')?old(
                    'meta_title'):$setting->meta_title }}</textarea>
                    <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                </div>

                <div class="form-group @error('meta_keyword') has-error @enderror">
                    <label for="meta_keyword">{{ trans('app.meta_keyword') }} </label>
                    <textarea name="meta_keyword" id="meta_keyword" class="form-control" placeholder="{{ trans('app.meta_keyword') }}">{{ old('meta_keyword')?old(
                    'meta_keyword'):$setting->meta_keyword }}</textarea>
                    <span class="text-danger">{{ $errors->first('meta_keyword') }}</span>
                </div>

                <div class="form-group @error('meta_description') has-error @enderror">
                    <label for="meta_description">{{ trans('app.meta_description') }} </label>
                    <textarea name="meta_description" id="meta_description" class="form-control" placeholder="{{ trans('app.meta_description') }}">{{ old('meta_description')?old(
                    'meta_description'):$setting->meta_description }}</textarea>
                    <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                </div>

                <div class="form-group @error('google_map') has-error @enderror">
                    <label for="google_map">{{ trans('app.google_map') }} </label>
                    <textarea name="google_map" id="google_map" class="form-control" placeholder="{{ trans('app.google_map') }}">{{ old('google_map')?old(
                    'google_map'):$setting->google_map }}</textarea>
                    <span class="text-danger">{{ $errors->first('google_map') }}</span>
                </div>

                <div class="form-group text-right">
                    <button class="button btn btn-info" type="reset"><span>{{ trans('app.reset') }}</span></button>
                    <button class="button btn btn-success" type="submit"><span>{{ trans('app.update') }}</span></button> 
                </div>
            </div>
            {{ Form::close() }}
        </div> 

        <!-- Website Slider-->
        <div class="col-sm-6 mt-1 pr-0">   
            <div class="panel panel-primary">
            <div class="panel-heading">
                <ul class="row list-inline m-0">
                    <li class="col-xs-8 p-0 text-left">
                        <h5>{{ trans('app.website_content') }}</h5>
                    </li>             
                    <li class="col-xs-4 p-0 text-right"> 
                        <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-id="" data-target=".contentModal"><i class='fa fa-plus'></i></button>
                    </li> 
                </ul> 
            </div>
            <div class="panel-body"> 
                <p class="text-danger p-1"><span class="label label-warning">Note</span>  This section contains website data, please change the data carefully!</p>
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ __('app.position') }}</th>
                            <th>{{ __('app.photo') }}</th>
                            <th>{{ __('app.menu') }}</th>
                            <th>{{ __('app.type') }}</th>
                            <th>{{ __('app.title') }}</th> 
                            <th><i class="fa fa-cogs"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contents as $content)
                        <tr>
                            <th>
                                <span class='label label-{{($content->position>0)?"info":"primary"}}'>{{ $content->position }}</span> 
                            </th>
                            <td><img src="{{ asset(($content->image?$content->image:'public/assets/img/icons/no_image.jpg')) }}" alt="Image" class="thumbnail mb-0" width="48" height="48"> </td>
                            <td>{{ $content->menu }}</td>
                            <td>
                                {!! ($content->type=='section'?("<span class=\"label label-primary\">Section</span>"):($content->type=='slider'?("<span class=\"label label-danger\">Slider</span>"):"<span class=\"label label-warning\">Page</span>")) !!}
                                {!! (($content->status==1)?"<span class='label label-success'>". trans('app.active') ."</span>":"<span class='label label-danger'>". trans('app.inactive') ."</span>") !!}
                            </td>
                            <td>{{ $content->title }}</td>
                            <td> 
                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-id="{{ $content->id }}" data-target=".contentModal"><i class="fa fa-edit"></i></button> 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div> 
    </div> 
</div> 

<!-- Contents -->
<div class="modal fade contentModal" tabindex="-1" role="dialog" aria-labelledby="contentModal">
  <div class="modal-dialog" role="document">
    {{ Form::open(['url' => 'admin/setting/website/content', 'files' => true, "id"=>"contentFrm"]) }}
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><strong></strong> {{ trans('app.website_content') }}</h4>
        </div>
        <div class="modal-body">
            <div class="alert"></div>
            <input type="hidden" name="id" /> 
            <div class="row">
                <div class="form-group col-xs-4">
                    <label for="type">{{ trans('app.type') }} <i class="text-danger">*</i></label>
                    {{ Form::select('type', ['section'=>'Section', 'slider'=>'Slider', 'page'=>'Page'], null , [ 'class'=>'form-control no-select', "id"=>'type', 'placeholder' => trans('app.select_option')]) }}
                    <span class="text-danger"></span>
                </div>
                <div class="form-group col-xs-4">
                    <label for="menu">{{ trans('app.menu') }} <i class="text-danger">*</i></label> 
                    <input type="text" name="menu" id="menu" class="form-control" placeholder="{{ trans('app.menu') }}" autocomplete="off">
                    <span class="text-danger"></span>
                </div>
                <div class="form-group col-xs-4">
                    <label for="position">{{ trans('app.position') }} <i class="text-danger">*</i></label> 
                    <input type="number" name="position" id="position" class="form-control" placeholder="{{ trans('app.position') }}" value="0">
                    <span class="text-danger"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="title1">{{ trans('app.title') }} <i class="text-danger">*</i></label> 
                <input type="text" name="title" id="title1" class="form-control" placeholder="{{ trans('app.title') }}">
                <span class="text-danger"></span>
            </div> 
            <div class="form-group">
                <label for="sub_title">{{ trans('app.sub_title') }} </label>
                <textarea name="sub_title" id="sub_title" class="form-control" placeholder="{{ trans('app.sub_title') }}"></textarea>
                <span class="text-danger"></span>
            </div> 
            <div class="form-group">
                <label for="description1">{{ trans('app.description') }} </label>
                <textarea name="description" id="description1" class="summernote form-control" placeholder="{{ trans('app.description') }}"></textarea>
                <span class="text-danger"></span>
            </div> 
            <div class="form-group">
                <label for="image">{{ trans('app.photo') }}</label>
                <img src="{{ asset('public/assets/img/icons/no_image.jpg') }}" alt="" class="thumbnail" style="max-width:100%;height:180px"> 
                <input type="hidden" name="old_image"> 
                <input type="file" name="image" id="image">
                <span class="text-danger"></span>
                <span class="help-block">Diamension: (1280x450)px</span>
            </div>
            <div class="form-group">
                <label for="url">{{ trans('app.url') }}</label> 
                <input type="text" name="url" id="url" class="form-control" placeholder="{{ trans('app.url') }}">
                <span class="text-danger"></span>
            </div>
            <div class="form-group">
                <label for="status">{{ trans('app.status') }} <i class="text-danger">*</i></label>
                <div id="status"> 
                    <label class="radio-inline">
                        <input type="radio" name="status" value="1" checked> {{ trans('app.active') }}
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="status" value="0"> {{ trans('app.deactive') }}
                    </label> 
                </div>
                <span class="text-danger"></span>
            </div> 

            <div class="form-group text-right">
                <button class="button btn btn-info" type="reset"><span>{{ trans('app.reset') }}</span></button>
                <button class="button btn btn-success" type="submit"><span>{{ trans('app.save') }}</span></button> 
            </div>            
        </div>
    </div>
    {{ Form::close() }}
  </div>
</div> 
@endsection

@push('scripts')
<script src="{{ url('public/assets/js/summernote.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
 
    $('body').on('paste, focus, blur', '#google_map', function(){ 
        var tag = '<iframe';
        var attr = 'src=';
        var str = $(this).val(); 
        var exists = str.indexOf(tag);  
        if (exists > -1) { 
            str  = str.split(tag)[1]; 
            str  = str.split(attr)[1];  
            str  = str.split(str[0])[1];  
            str  = str.replace(/\s/g, ''); 
        }  
        $(this).val(str);
    }); 
 

     $('.summernote').summernote({
        height: ($(window).height() - 440),
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            ['view', ['fullscreen', 'codeview']]
        ] 
    });


    // ready modal form
    $('.contentModal').on('show.bs.modal', function (event) {
        var button    = $(event.relatedTarget);
        var id        = button.data('id');
        var modal     = $(this);
        modal.find('form').get(0).reset();
        modal.find('.alert').hide();
        modal.find('.form-group').removeClass('has-error');
        modal.find('.form-group').find('span.text-danger').html('');
        modal.find('input[name=id]').val('');
        modal.find('input[name=old_image]').val(''); 
        modal.find('img').attr('src', "{{ asset('public/assets/img/icons/no_image.jpg') }}");
        modal.find('.modal-title strong').html("<i class='fa fa-plus'></i>");
        modal.find('.button[type=submit]').text("{{ trans('app.save') }}");

        if (id != "") {
            modal.find('input[name=id]').val(id);
            modal.find('.modal-title strong').html("<i class='fa fa-pencil'></i>");
            modal.find('button[type=submit]').text("{{ trans('app.update') }}");

            $.ajax({
                url        : '{{ url("admin/setting/website/content") }}/'+id,
                type       : 'get',
                dataType   : 'json',
                success: function(response) {
                    var res = response.data;
                    if (response.status) {
                        modal.find('[name=id]').val(res.id);
                        modal.find('[name=type]').val(res.type);
                        modal.find('[name=menu]').val(res.menu);
                        modal.find('[name=title]').val(res.title);
                        modal.find('[name=sub_title]').val(res.sub_title);
                        modal.find('[name=description]').code(res.description);
                        modal.find('[name=old_image]').val(res.image); 
                        modal.find('[name=url]').val(res.url);
                        modal.find('[name=position]').val(res.position);
                        modal.find('[name=status][value="'+res.status+'"]').prop('checked', true); 

                        if (res.image && res.image!='' && res.image!==null) {
                            modal.find('img').attr('src', ('{{ asset('') }}'+res.image));
                        }
                    }
                },
                error: function(xhr) {
                    modal.find('.alert').addClass('alert-danger').removeClass('alert-success').show().html('Internal server error! failed to get the website content');
                }
            });
        } 
    });

    // generate page slug
    $('body').on('change', '.contentModal #type', function(){
        var type = $(this).val();
        var menu = type;
        if (type=='page') {
            $.get('{{ url("admin/setting/website/slug") }}', function( data ) {
                $(".contentModal #menu").val(data);
            });
        } else {
            $(".contentModal #menu").val(menu);
        }
    });
    $('body').on('focus, blur', '.contentModal #menu', function(){
        var type = $('.contentModal #type').val();
        var id   = $('.contentModal [name=id]').val();
        if (type=='page') {
            var menu = $(this).val();
            $.get('{{ url("admin/setting/website/slug") }}?menu='+menu+'&id='+id, function( data ) {
                $('.contentModal #menu').val(data);
            });
        }
    });

    // preview image
    $('body').on('change', '.contentModal #image', function() {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.contentModal img').attr('src', e.target.result);
            } 
            /* convert to base64 string*/
            reader.readAsDataURL(input.files[0]);
        }
    });

    // submit form
    $("#contentFrm").on('submit', function(e){
        e.preventDefault();

        var form = $(this);
        form.find('.form-group').removeClass('has-error');
        form.find('.form-group span.text-danger').html('');

        $.ajax({
            url        : form.attr('action'),
            type       : form.attr('method'),
            dataType   : 'json',
            data       : new FormData(form[0]),
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.status==false) {
                    $.each(response.data, function(field, message){
                        var input = form.find('[name="'+field+'"]');
                        input.closest('.form-group').addClass('has-error');
                        input.closest('.form-group').find('span.text-danger').html(message);
                    });

                    form.find('.alert').addClass('alert-danger').removeClass('alert-success').show().html(response.message);
                } else {
                    form.find('.alert').addClass('alert-success').removeClass('alert-danger').show().html(response.message);
                    setInterval(function(){
                        window.history.go(0);
                    }, 3000);
                }
            },
            error: function(xhr) {
                form.find('.alert').addClass('alert-danger').removeClass('alert-success').show().html('Internal server error! failed to update the website content');
            }
        });
    });
});
</script>
@endpush