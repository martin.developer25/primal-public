@extends('layouts.frontend')

@push('styles')
<!-- owl-carousel -->
<link href="{{ asset('public/assets/css/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
@endpush
 
@section('content')
    <!-- home -->
    @if (!empty($contents->sliders))
    <section id="home">
        <div class="container-fluid">
            <div class="row"> 
                <div id="home-slide" class="leading col-sm-12 thm-padding owl-carousel owl-theme">
                    @foreach($contents->sliders as $slide)
                    <div class="home-item item">
                        <div class="home-image-wrapper">
                            <img class="img-responsive" src="{{ asset(!empty($slide->image)?$slide->image:'public/assets/img/icons/no_image.jpg') }}" alt="">
                        </div>
                        <div class="trailer-info">
                            <div class="trailer-info-block">
                                <h3><a href="javascript:void(0)">{{ $slide->title }}</a></h3>
                                @if ($slide->sub_title)
                                <h4 class="genry">{{ $slide->sub_title }}</h4>
                                @endif
                                @if ($slide->description)
                                <p class="genry">{!! $slide->description !!}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach 
                </div>
            </div>
        </div>
    </section>
    @endif

    @foreach($contents->sections as $key => $section)
        <!-- get started --> 
        @if ($key == 'get-started')
        <section id="get-started">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-content">
                            <div class="header-content-inner">
                                <h1>{{ $section->title }}</h1> 
                                @if(!empty($section->sub_title))
                                <p>{!! $section->sub_title !!}</p>
                                @endif
                                @if(!empty($section->description))
                                <p>{!! $section->description !!}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-area">  
                            <div class="form-head">
                                <h3 class="text-uppercase">{{ $section->menu }}!</h3>
                                <h4>{{ trans('app.join_the_our_network') }}</h4>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active text-uppercase"><a href="#signin" aria-controls="signin" role="tab" data-toggle="tab">{{ trans('app.signin') }}</a></li>
                                    <li role="presentation" class="text-uppercase"><a href="#signup" aria-controls="signup" role="tab" data-toggle="tab">{{ trans('app.signup') }}</a></li> 
                                    <li role="presentation" class="text-uppercase"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">{{ trans('app.forgot_password') }}</a></li> 
                                </ul>
                            </div>
                            <div class="form-body">
                                <div class="alert mb-0 hidden"></div>
                                <div class="tab-content">
                                    <!-- signin -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="signin">
                                        {{ Form::open(['data-url'=>'client/signin', 'id'=>'signinFrm']) }}

                                            <div class="form-group">
                                                <input name="email" type="email" placeholder="{{ trans('app.email') }}" class="form-control" id="in-email">
                                                <label for="in-email">{{ trans('app.email') }}</label>
                                                <span class="help-block"></span>
                                            </div> 

                                            <div class="form-group">
                                                <input name="password" type="password" placeholder="{{ trans('app.password') }}" class="form-control" id="in-password">
                                                <label for="in-password">{{ trans('app.password') }}</label>
                                                <span class="help-block"></span>
                                            </div>   
                                            
                                            <div class="clear"></div>                                
                                            <div class="btn-group">
                                                <button type="submit" class="btn thm-btn form-btn">{{ trans('app.signin') }}</button>
                                            </div>
                                        {{ Form::close() }}

                                        @include('backend.common.info')
                                        @yield('info.client-credentials')
                                    </div>
                                    <!-- signup -->
                                    <div role="tabpanel" class="tab-pane fade" id="signup">
                                        {{ Form::open(['data-url'=>'client/signup', 'id'=>'signupFrm']) }}

                                            <div class="row clear">
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <input name="firstname" type="text" placeholder="{{ trans('app.firstname') }}" class="form-control" id="up-firstname">
                                                        <label for="up-firstname">{{ trans('app.firstname') }}</label>
                                                        <span class="help-block"></span>
                                                    </div>  
                                                </div>
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <input name="lastname" type="text" placeholder="{{ trans('app.lastname') }}" class="form-control" id="up-lastname">
                                                        <label for="up-lastname">{{ trans('app.lastname') }}</label>
                                                        <span class="help-block"></span>
                                                    </div> 
                                                </div> 
                                            </div>  

                                            <div class="form-group">
                                                <input name="email" type="email" placeholder="{{ trans('app.email') }}" class="form-control" id="up-email">
                                                <label for="up-email">{{ trans('app.email') }}</label>
                                                <span class="help-block"></span>
                                            </div> 
   

                                            <div class="row clear">
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <input name="password" type="password" placeholder="{{ trans('app.password') }}" class="form-control" id="up-password">
                                                        <label for="up-password">{{ trans('app.password') }}</label>
                                                        <span class="help-block"></span>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input name="conf_password" type="password" placeholder="{{ trans('app.conf_password') }}" class="form-control" id="up-conf_password">
                                                        <label for="up-conf_password">{{ trans('app.conf_password') }}</label>
                                                        <span class="help-block"></span>
                                                    </div>  
                                                </div> 
                                            </div> 
                                            
                                            <div class="clear"></div>
                                            <div class="btn-group">
                                                <button type="submit" class="btn thm-btn form-btn">{{ trans('app.signup') }}</button>
                                            </div>
                                        {{ Form::close() }}
                                    </div> 
                                    <!-- password -->
                                    <div role="tabpanel" class="tab-pane fade" id="password">
                                        {{ Form::open(['data-url'=>'client/forgot-password', 'id'=>'passwordFrm']) }}  
                                            <div class="form-group">
                                                <input name="email" type="email" placeholder="{{ trans('app.email') }}" class="form-control" id="pass-email">
                                                <label for="pass-email">{{ trans('app.email') }}</label>
                                                <span class="help-block"></span>
                                            </div> 
                                            <div class="clear"></div>
                                            <div class="btn-group">
                                                <button type="submit" class="btn thm-btn form-btn">{{ trans('app.submit') }}</button>
                                            </div> 
                                        {{ Form::close() }}
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- explore section -->
        @elseif ($key == 'explore')
        <section id="explore">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="even-title">
                            <h1>{{ $section->title }}</h1>
                            @if(!empty($section->sub_title))
                            <p>{{ $section->sub_title }}</p>
                            @endif 
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    @if(!empty($counters))
                    @foreach($counters as $title => $count)
                        <div class="explore-wiget">
                            <div class="explore-text">
                                <h3><span class="count">{{ $count }}+ </span><br/> {{ $title }}</h3>
                            </div>
                        </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </section>

        <!-- About Us Section -->
        @elseif ($key == 'about-us')
        <section id="about-us">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="about-us-text">
                            <h5 class="text-uppercase">{{ $section->menu }}</h5>
                            <h1>{{ $section->title }}</h1>
                            @if(!empty($section->sub_title))
                            <p>{{ $section->sub_title }}</p>
                            @endif
                            @if(!empty($section->description))
                            <p>{{ $section->description }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="about-us-img">
                            <img src="{{ asset(!empty($section->image)?$section->image:'public/assets/img/icons/no_image.jpg') }}" class="img-responsive center-block" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- experts -->
        @elseif ($key == 'experts')
        <section id="experts">
            <div class="container">
                <div class="row">
                    <h2>{{ $section->title }}</h2>
                    <div id="experts-slide" class="owl-carousel owl-theme">
                        @foreach($officers as $officer)
                        <div class="item">
                            <div class="tm-text">
                                <img src="{{ asset((!empty($officer->photo)?$officer->photo:'public/assets/img/icons/no_user.jpg')) }}" class="img-responsive center-block" alt="">
                                <div class="name">{{ $officer->firstname }} {{ $officer->lastname }}</div>
                                <div class="auther">{{ (!empty($officer->department->name)?$officer->department->name:(!empty($officer->user_type==5)?__('app.admin'):null)) }}</div>
                                <p>{{ $officer->about }}</p>
                            </div>
                        </div>
                        @endforeach 
                    </div>
                    <p class="text-center mt-1">
                        <a class="btn thm-btn page-scroll mt-3" href="#get-started">Get started</a>
                    </p>
                </div>
            </div>
        </section>

        <!-- contact-us --> 
        @elseif ($key == 'contact-us')
        <section id="contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="about-us-text">
                            <h5 class="text-uppercase">{{ $section->menu }}</h5> 
                            <h4>{{ $app->title }}</h4>
                            <p>{!! $app->description !!}</p>
                            <h4>{{ trans('app.address') }}</h4>
                            <p>{{ $app->address }}</p>
                            <h4>{{ trans('app.email') }}</h4>
                            <p>{{ $app->email }}</p> 
                            <h4>{{ trans('app.mobile') }}</h4>
                            <p>{{ $app->phone }}</p> 
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="about-us-img">
                            @if ($app->google_map)
                                <iframe src="{!! $app->google_map !!}" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            @else
                                <img src="{{ asset(!empty($section->image)?$section->image:'public/assets/img/icons/map.PNG') }}" alt="Google Embeded Map" class="img-responsive" /> 
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section> 

        <!-- Other Section -->
        @else
        <section id="{{$key}}" class="common-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="about-us-img">
                            <img src="{{ asset(!empty($section->image)?$section->image:'public/assets/img/icons/no_image.jpg') }}" class="img-responsive center-block" alt=""/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="about-us-text">
                            <h5 class="text-uppercase">{{ $section->menu }}</h5>
                            <h1>{{ $section->title }}</h1>
                            @if(!empty($section->sub_title))
                            <p>{{ $section->sub_title }}</p>
                            @endif
                            @if(!empty($section->description))
                            <p>{{ $section->description }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section> 
        @endif 
    @endforeach 
@endsection
 
@push('scripts')
<script src="{{ asset('public/assets/js/owl.carousel.min.js') }}"></script> 
<script type="text/javascript">
$(document).ready(function () {   
    // home-slider
    $("#home-slide").owlCarousel({
        navigation: false,
        pagination: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        navigationText: [
            "<i class='fa fa-chevron-left'></i>",
            "<i class='fa fa-chevron-right'></i>"
        ]
    });

    // experts slideshow
    $("#experts-slide").owlCarousel({
        navigation: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true
    }); 

    // counter
    var played = false;
    $(window).scroll(function() {
        var el       = document.querySelector("#explore");
        var position = el.getBoundingClientRect().top;
        if (!played && position <= 60 && position >= -400 ) {
            $('.count').each(function () {
                var $this = $(this);
                $({ counter: 0 }).animate({ counter: ($this.text()).replace(/[^0-9]/g,'') }, {
                    duration: 1000,
                    easing: 'swing',
                    step: function () {
                        $this.text(Math.ceil(this.counter)+"+");
                    }
                });
            });
            played = true;
        } 
    }); 

    // tabs 
    $('.form-area .nav-tabs li>a').on('click', function () { 
        $('.form-area').find('.alert').addClass('hidden').html(''); 
    })

    // signin
    $('#signinFrm').on('submit', function(e){
        e.preventDefault(); 
        var form = $(this);
        form.find('.input').removeClass('has-error');
        form.find('.input').next('.help-block').html('');

        $.ajax({
            url        : form.attr('data-url'),
            type       : form.attr('method'),
            dataType   : 'json',
            data       : $(this).serializeArray(),              
            beforeSend: function () {
                form.closest('.form-body').find('.alert').removeClass('hidden alert-success alert-danger').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
            },
            success: function(response) {  
                if (response.status==false) {
                    $.each(response.data, function(field, message){  
                        var input = form.find('[name="'+field+'"]'); 
                        input.closest('.form-group').addClass('has-error');
                        input.closest('.form-group').find('.help-block').html(message);
                    });

                    form.closest('.form-body').find('.alert').addClass('alert-danger').removeClass('hidden alert-success').show().html(response.message);
                } else {
                    form.closest('.form-body').find('.alert').addClass('alert-success').removeClass('hidden alert-danger').html(response.message);
                    setInterval(function(){ 
                        window.location.href = $('meta[name=url]').attr('content')+"/client/token";
                    }, 2000);
                }
            },
            error: function(xhr) {
                form.closest('.form-body').find('.alert').addClass('alert-danger').removeClass('hidden alert-success').html('Internal server error! failed to update the website content');
            }
        });
    });

    // signup
    $('#signupFrm').on('submit', function(e){
        e.preventDefault(); 
        var form = $(this);
        form.find('.input').removeClass('has-error');
        form.find('.input').next('.help-block').html('');

        $.ajax({
            url        : form.attr('data-url'),
            type       : form.attr('method'),
            dataType   : 'json',
            data       : $(this).serializeArray(),                
            beforeSend: function () {
                form.closest('.form-body').find('.alert').removeClass('hidden alert-success alert-danger').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
            },
            success: function(response) { 
                if (response.status==false) {   
                    $.each(response.data, function(field, message){  
                        var input = form.find('[name="'+field+'"]'); 
                        input.closest('.form-group').addClass('has-error');
                        input.closest('.form-group').find('.help-block').html(message);
                    });

                    form.closest('.form-body').find('.alert').addClass('alert-danger').removeClass('hidden alert-success').show().html(response.message);
                } else {
                    form.closest('.form-body').find('.alert').addClass('alert-success').removeClass('hidden alert-danger').html(response.message);
                    setInterval(function(){ 
                        window.location.href = $('meta[name=url]').attr('content');
                    }, 30000);
                }
            },
            error: function(xhr) {
                form.closest('.form-body').find('.alert').addClass('alert-danger').removeClass('hidden alert-success').html('Internal server error!');
            }
        });
    });

    // password
    $('#passwordFrm').on('submit', function(e){
        e.preventDefault(); 
        var form = $(this);
        form.find('.input').removeClass('has-error');
        form.find('.input').next('.help-block').html('');

        $.ajax({
            url        : form.attr('data-url'),
            type       : form.attr('method'),
            dataType   : 'json',
            data       : $(this).serializeArray(),              
            beforeSend: function () {
                form.closest('.form-body').find('.alert').removeClass('hidden alert-success alert-danger').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
            },
            success: function(response) { 
                if (response.status==false) { 
                    $.each(response.data, function(field, message){  
                        var input = form.find('[name="'+field+'"]'); 
                        input.closest('.form-group').addClass('has-error');
                        input.closest('.form-group').find('.help-block').html(message);
                    });

                    form.closest('.form-body').find('.alert').addClass('alert-danger').removeClass('hidden alert-success').show().html(response.message);
                } else {
                    form.closest('.form-body').find('.alert').addClass('alert-success').removeClass('hidden alert-danger').html(response.message);
                    setInterval(function(){ 
                        window.location.href = $('meta[name=url]').attr('content');
                    }, 30000);
                }
            },
            error: function(xhr) {
                form.closest('.form-body').find('.alert').addClass('alert-danger').removeClass('hidden alert-success').html('Internal server error!');
            }
        });
    });
});
</script>
@endpush