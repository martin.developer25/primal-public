@extends('layouts.frontend')
 
@section('content')
    <!-- Page -->
    @if (!empty($page))
    <section id="{{ strtolower(str_replace(' ', '-', $page->menu)) }}" class="common-section">
        <div class="container">
            <div class="row"> 
                <div class="col-sm-12">
                    <div class="about-us-text">
                        <h5 class="text-uppercase">{{ $page->menu }}</h5>
                        <h1>{{ $page->title }}</h1>
                        @if(!empty($page->sub_title))
                        <p>{{ $page->sub_title }}</p>
                        @endif
                        <br/>
                        <br/>
                        <div class="row"> 
	                        @if (!empty($page->image))
	                        <div class="col-sm-4">
		                        <img src="{{ asset($page->image) }}" class="thumbnail center-block" style="max-height:250px" alt=""/>
		                    </div>
	                        @endif

	                        @if(!empty($page->description))
	                        <div class="col-sm-8">
	                        	<p>{{ $page->description }}</p>
		                    </div>
	                        @endif

                            @if(!empty($page->url))
                            <p class="about-us-text">
                                <a class="btn thm-btn" href="{{ $page->url }}">{{ trans('app.visit_now') }}</a>
                            </p>
                            @endif
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection
