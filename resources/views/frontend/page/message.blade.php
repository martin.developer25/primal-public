@extends('layouts.frontend')

@push('styles')
<style type="text/css"> 
.card {margin:0 auto;width:320px;}  
.card .panel-heading{background:#8bc34a;color:#fff;padding:3em 0 2em 0;text-align:center;text-transform:uppercase;font-size:20px;}
.card .panel-heading i{font-size:72px;}
.card .panel-body {color:#757575;font-size:18px;text-align:center;letter-spacing:1px} 
.card .panel-body p{padding:1em 0} 
.card .btn {color:#fff;} 
.card.success .panel-heading{background:#8bc34a;}
.card.failed .panel-heading{background:#ff2d20;}
.card.success .btn{border-color:#6b8a47;background:#8bc34a;} 
.card.failed .btn{border-color: #d43f3a;background:#ff2d20}
</style>
@endpush
 
@section('content') 
<section id="welcome" class="common-section">  
    <div class="container"> 
        <div class="about-us-text">
            <h5 class="text-uppercase">{{ trans('app.message') }}</h5>
            <h1>{{ !empty($title)?$title:null }}</h1> 
        </div> 


        <div class="card {{ (!empty($status)?'success':'failed') }} panel panel-default"> 
            <div class="panel-heading"> 
                @if(!empty($status))
                <i class="glyphicon glyphicon-ok-circle"></i>
                <p>{{ trans('app.success') }}</p>
                @else
                <i class="glyphicon glyphicon-remove-circle"></i> 
                <p>{{ trans('app.failed') }}</p>
                @endif
            </div> 
            <div class="panel-body">
                <p>{{ (!empty($message)?$message:null) }}</p>
                <a href="{{ url(!empty($status)?('/#get-started'):('/')) }}" class="btn btn-lg btn-default">{{ trans('app.continue') }}</a>
            </div>
        </div> 
    </div>
</section> 
@endsection
