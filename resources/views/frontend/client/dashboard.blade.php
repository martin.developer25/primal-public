@extends('layouts.frontend')
 
@section('content')
<!-- Page --> 
<section id="dashboard" class="common-section">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12">
                <div class="about-us-text">
                    <h5 class="text-uppercase">{{ trans('app.dashboard') }}</h5> 
                    <div class="row">     
                        <div class="col-sm-12"> 
                            <h1 class="text-center mb-0">{{ trans('app.this_month') }}</h1>
                            <div class="panel-body"><canvas id="lineChart" style="height:200px"></canvas></div>
                        </div> 
                        
                        <div class="col-sm-6"> 
                            <h1 class="text-center mb-0">{{ trans('app.this_year') }}</h1>
                            <div class="panel-body"><canvas id="singelBarChart" style="height:200px"></canvas></div>
                        </div>
                        
                        <div class="col-sm-6">
                            <h1 class="text-center mb-0">{{ trans('app.from_the_begining') }}</h1>
                            <div class="panel-body"><canvas id="pieChart" style="height:200px"></canvas></div>
                        </div> 

                        <div class="col-sm-12 table-responsive">
                            <h1 class="text-center mb-0">{{ trans('app.from_the_begining') }}</h1>
                            <table class="table table-bordered text-center">
                                <thead class="bg-info">
                                    <tr>
                                        <th>{{ trans('app.status') }}</th>   
                                        <td>{{ trans('app.token') }}</td> 
                                    </tr>
                                </thead> 
                                <tbody>
                                    @php     
                                        $tokenPending = (!empty($charts->token['0'])?$charts->token['0']:0);
                                        $tokenComplete = (!empty($charts->token['1'])?$charts->token['1']:0);
                                        $tokenStop    = (!empty($charts->token['2'])?$charts->token['2']:0);

                                        $totalToken = $tokenPending+$tokenComplete+$tokenStop; 
                                    @endphp
                                    <tr>
                                        <th scope="row" class="bg-warning">{{ trans('app.pending') }}</th>   
                                        <td class="bg-info">{{ $tokenPending }}</td> 
                                    </tr>
                                    <tr>
                                        <th scope="row" class="bg-success">{{ trans('app.complete') }}</th>     
                                        <td class="bg-info">{{ $tokenComplete }}</td> 
                                    </tr> 
                                    <tr>
                                        <th scope="row" class="bg-danger">{{ trans('app.stop') }}</th>  
                                        <td class="bg-info">{{ $tokenStop }}</td> 
                                    </tr> 
                                    <tr>
                                        <th scope="row" class="bg-info">{{ trans('app.total') }}</th>  
                                        <td class="bg-info">{{ $totalToken }}</td> 
                                    </tr> 
                                </tbody> 
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
@endsection
@push('scripts')
<script src="{{ asset('public/assets/js/Chart.min.js') }}"></script>
<script type="text/javascript"> 
$(window).on('load', function(){

    //line chart
    var ctx = document.getElementById("lineChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                <?php 
                if (!empty($charts->month)) {
                    for ($i=0; $i < sizeof($charts->month) ; $i++) { 
                       echo (!empty($charts->month[$i])?$charts->month[$i]->date:0).", ";
                    }
                }
                ?>
            ],
            datasets: [
                {
                    label: "Total",
                    borderColor: "rgba(24, 97, 142, .9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(24, 97, 142, .09)",
                    pointHighlightStroke: "rgba(24, 97, 142, 1)",
                    data: [
                        <?php 
                        if (!empty($charts->month)) {
                            for ($i=0; $i < sizeof($charts->month) ; $i++) { 
                               echo (!empty($charts->month[$i])?$charts->month[$i]->total:0).", ";
                            }
                        }
                        ?>
                    ]
                },
                {
                    label: "Success",
                    borderColor: "rgba(225, 48, 91, 0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(225, 48, 91, 0.09)",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [
                        <?php 
                        if (!empty($charts->month)) {
                            for ($i=0; $i < sizeof($charts->month) ; $i++) { 
                               echo (!empty($charts->month[$i])?$charts->month[$i]->success:0).", ";
                            }
                        }
                        ?>
                    ]
                },
                {
                    label: "Pending",
                    borderColor: "rgba(0,0,0, 0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(0,0,0, 0.09)",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [
                        <?php 
                        if (!empty($charts->month)) {
                            for ($i=0; $i < sizeof($charts->month) ; $i++) { 
                               echo (!empty($charts->month[$i])?$charts->month[$i]->pending:0).", ";
                            }
                        }
                        ?>
                    ]
                }
            ]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            } 
        }
    });


    // single bar chart
    var ctx = document.getElementById("singelBarChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                <?php 
                if (!empty($charts->year)) {
                    for ($i=0; $i < sizeof($charts->year) ; $i++) { 
                       echo "'".(!empty($charts->year[$i])?$charts->year[$i]->month:0)."', ";
                    }
                }
                ?>
            ],
            datasets: [
                {
                    label: "Total",
                    borderColor: "rgba(24, 97, 142, 0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(24, 97, 142, 0.5)",
                    data: [
                        <?php 
                        if (!empty($charts->year)) {
                            for ($i=0; $i < sizeof($charts->year) ; $i++) { 
                               echo (!empty($charts->year[$i])?$charts->year[$i]->total:0).", ";
                            }
                        }
                        ?>
                    ]
                },
                {
                    label: "Success",
                    borderColor: "rgba(225, 48, 91, 0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(225, 48, 91, 0.5)",
                    data: [
                        <?php 
                        if (!empty($charts->year)) {
                            for ($i=0; $i < sizeof($charts->year) ; $i++) { 
                               echo (!empty($charts->year[$i])?$charts->year[$i]->success:0).", ";
                            }
                        }
                        ?>
                    ]
                },
                {
                    label: "Pending",
                    borderColor: "rgba(0,0,0, 0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(0,0,0, 0.5)",
                    data: [
                        <?php 
                        if (!empty($charts->year)) {
                            for ($i=0; $i < sizeof($charts->year) ; $i++) { 
                               echo (!empty($charts->year[$i])?$charts->year[$i]->pending:0).", ";
                            }
                        }
                        ?>
                    ]
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }             
        }
    });



    // pie chart
    var ctx = document.getElementById("pieChart"); 
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [
                        <?php 
                        if (!empty($charts->begin) && is_array($charts->begin)) { 
                               echo (!empty($charts->begin[0])?$charts->begin[0]->total:0).", ";
                               echo (!empty($charts->begin[0])?$charts->begin[0]->success:0).", ";
                               echo (!empty($charts->begin[0])?$charts->begin[0]->pending:0); 
                        }
                        ?>
                    ],
                    backgroundColor: [
                        "rgba(24, 97, 142,0.9)",
                        "rgba(225, 48, 91,0.7)",
                        "rgba(0,0,0,0.5)",
                        "rgba(0,0,0,0.07)"
                    ],
                    hoverBackgroundColor: [
                        "rgba(24, 97, 142,0.9)",
                        "rgba(225, 48, 91,0.7)",
                        "rgba(0,0,0,0.5)",
                        "rgba(0,0,0,0.07)"
                    ]

                }],
            labels: [
                "Total",
                "Success",
                "Pending"
            ]
        },
        options: {
            responsive: true
        }
    });
 
});
</script>
@endpush
