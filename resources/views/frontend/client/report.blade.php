@extends('layouts.frontend')

@push('styles')
<link href="{{ asset('public/assets/css/jquery-ui.min.css') }}" rel='stylesheet'>
<link href="{{ asset('public/assets/css/select2.min.css') }}" rel='stylesheet'>
<link href="{{ asset('public/assets/css/dataTables.min.css') }}" rel='stylesheet'>
@endpush
 

@section('content')
<!-- Page --> 
<section id="dashboard" class="common-section">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12">
                <div class="about-us-text">
                    <h5 class="text-uppercase">{{ trans('app.reports') }}</h5>  
                </div>
            </div>
            
            <div class="col-sm-12">
                @include('backend.common.info')
                @yield('info.message')  
            </div>

            <div class="col-sm-12 table-responsive">
                <table class="dataTables table table-bordered" width="100%" cellspacing="0">
                    <thead class="bg-info"> 
                        <tr>
                            <th rowspan="2">#</th> 
                            <td width="100"> 
                                <input type="text" class="datepicker input-sm filter mb-1" id="start_date" placeholder="{{ trans('app.start_date') }}" autocomplete="off" /> 
                                <input type="text" class="datepicker input-sm filter" id="end_date" placeholder="{{ trans('app.end_date') }}" autocomplete="off"/>
                            </td>
                            <td> 
                                {{ Form::select('status', ["'0'"=>trans("app.pending"), '1'=>trans("app.complete"), '2'=>trans("app.stop")],  null,  ['placeholder' => trans("app.status"), 'id'=> 'status', 'class'=>'select2 filter input-sm']) }} 
                            </td> 
                            <td> 
                                {{ Form::select('department', $departments, null, ['id'=>'department', 'class'=>'select2 filter input-sm', 'placeholder'=> trans('app.department')]) }} 
                            </td>  
                            <td> 
                                {{ Form::select('counter', $counters, null, ['id'=>'counter', 'class'=>'select2 filter input-sm', 'placeholder'=> trans('app.counter')]) }} 
                            </td>   
                            <td> 
                                {{ Form::select('officer', $officers, null, ['id'=>'officer', 'class'=>'select2 filter input-sm', 'placeholder'=> trans('app.officer')]) }} 
                            </td>    
                            <td></td> 
                            <td></td> 
                        </tr> 
                        <tr> 
                            <th>{{ trans('app.token_no') }}</th> 
                            <th>{{ trans('app.status') }}</th>
                            <th>{{ trans('app.department') }}</th>
                            <th>{{ trans('app.counter') }}</th>
                            <th>{{ trans('app.officer') }}</th> 
                            <th>{{ trans('app.details') }}</th> 
                            <th>{{ trans('app.action') }}</th>
                        </tr> 
                    </thead>  
                </table>  
            </div>
        </div>
    </div>
</section> 


<!-- Preview Modal --> 
<div class="modal fade previewModal"  tabindex="-1" role="dialog" aria-labelledby="previewModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="previewModalLabel">{{ trans('app.token') }}</h4>
      </div>
      <div class="modal-body text-center"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('public/assets/js/jquery-ui.min.js') }}"></script> 
<script src="{{ asset('public/assets/js/dataTables.min.js') }}"></script> 
<script src="{{ asset('public/assets/js/select2.min.js') }}"></script>
<script src="{{ asset('public/assets/js/fastclick.min.js') }}"></script>
<script> 
(function(){
    // DATATABLE
    drawDataTable();
    // setInterval(function(){
    //     drawDataTable();
    // }, 10000);

    $("body").on("change",".filter", function(){
        drawDataTable();
    });

    function drawDataTable()
    {   
        $('.dataTables').DataTable().destroy();
        $('.dataTables').DataTable({
            responsive: true, 
            processing: true,
            serverSide: true,
            ajax: {
                url:'<?= route('client.token.data'); ?>',
                dataType: 'json',
                type    : 'post',
                data    : {
                    _token : '{{ csrf_token() }}', 
                    search: {
                        status     : $('#status').val(),
                        counter    : $('#counter').val(),
                        department : $('#department').val(),
                        officer    : $('#officer').val(),
                        start_date : $('#start_date').val(),
                        end_date   : $('#end_date').val(),
                    }
                }
            },
            columns: [ 
                { data: 'serial' },
                { data: 'token_no' },
                { data: 'status' },
                { data: 'department' },
                { data: 'counter' },
                { data: 'officer' }, 
                { data: 'details' }, 
                { data: 'options' }  
            ],  
            order: [ [0, 'desc'] ], 
            select    : true,
            pagingType: "full_numbers",
            lengthMenu: [[25, 50, 100, 150, 200, 500, -1], [25, 50, 100, 150, 200, 500, "All"]],
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>><'row'<'col-sm-12't>><'row'<'col-sm-6'i><'col-sm-6'p>>", 
            columnDefs: [
                { "orderable": false, "targets": [7] }
            ], 
            buttons: [
                { extend:'copy', text:'<i class="fa fa-copy"></i>', className:'btn-sm btn-info',exportOptions:{columns:':visible'}},
                { extend: 'print', text  :'<i class="fa fa-print"></i>', className:'btn-sm btn-info', exportOptions: { columns: ':visible',  modifier: { selected: null } }},  
                { extend: 'print', text:'<i class="fa fa-print"></i>  Selected', className:'btn-sm btn-info', exportOptions:{columns: ':visible'}},  
                { extend:'excel',  text:'<i class="fa fa-file-excel-o"></i>', className:'btn-sm btn-info',exportOptions:{columns:':visible'}},
                { extend:'pdf',  text:'<i class="fa fa-file-pdf-o"></i>',  className:'btn-sm btn-info',exportOptions:{columns:':visible'}}
            ] 
        });   
    } 

    // print token
    $("body").on("click", ".tokenPrint", function(e) { 
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type:'POST',
            dataType: 'json',
            data: {
                'id' : $(this).attr('data-token-id'),
                '_token':'<?php echo csrf_token() ?>'
            },
            success:function(data)
            {
                var content = "<style type=\"text/css\">@media print {"+
                       "html, body {display:block;margin:0!important; padding:0 !important;overflow:hidden;display:table;}"+
                       ".receipt-token {width:100vw;height:100vw;text-align:center}"+
                       ".receipt-token h4{margin:0;padding:0;font-size:7vw;line-height:7vw;text-align:center}"+
                       ".receipt-token h1{margin:0;padding:0;font-size:15vw;line-height:20vw;text-align:center}"+
                       ".receipt-token ul{margin:0;padding:0;font-size:7vw;line-height:8vw;text-align:center;list-style:none;}"+
                       ".receipt-token img{width:500px}"+
                       "}</style>";
                content += "<div class=\"receipt-token\">";
                content += "<img src='{{ asset('public/assets/img/icons/logo.jpg') }}' alt='logo'/>";
                content += "<h4>{{ \Session::get('app.title') }}</h4>";
                content += "<h1>"+data.token_no+"</h1>";
                content +="<ul class=\"list-unstyled\">";
                content += "<li><strong>{{ trans('app.department') }} </strong>"+data.department+"</li>";
                content += "<li><strong>{{ trans('app.counter') }} </strong>"+data.counter+"</li>";
                content += "<li><strong>{{ trans('app.officer') }} </strong>"+data.firstname+' '+data.lastname+"</li>";
                if (data.note)
                {
                    content += "<li><strong>{{ trans('app.note') }} </strong>"+data.note+"</li>";
                }
                content += "<li><strong>{{ trans('app.date') }} </strong>"+data.created_at+"</li>";
                content += "</ul>";  
                content += "</div>";    
     
                // print 
                // printThis(content); 
                $('.previewModal').modal('show').find('.modal-body').html(content);

            }, error:function(err){
                alert('failed!');
            }
        });  
    });
  
    // datepicker
    $(".datepicker").datepicker({
        dateFormat: "mm/dd/yy",
        changeMonth: true,
        changeYear: true
    });

    // enable FastClick 
    FastClick.attach(document.body); 
    
    // select 2 dropdown  
    // var $customSelects = $('select:not(.no-select)'); 
    // $customSelects.select2({ 
    //     templateResult: function(result, container) {
    //         if (!result.id) {
    //             return result.text;
    //         }
    //         container.className += ' needsclick';
    //         return result.text;
    //     },
    //     placeholder: 'Select Option',
    //     allowClear: true
    // });
    // // add needsclick to all element of the select2 for supports in IOS-ANDROID
    // $customSelects.each(function(index, el){
    //     $(el).data('select2').$container.find('*').addClass('needsclick');
    // });  

})(); 
</script>
@endpush