@extends('layouts.frontend')

@section('content')  
<section id="token" class="common-section"> 
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12">
                <div class="about-us-text">
                    <h5 class="text-uppercase">{{ trans('app.token') }}</h5> 
                </div>
            </div>
  
            <div class="col-sm-6" style="margin-bottom: 120px;">  
                @if(empty(auth()->user()->mobile) && !empty($tokens->display->show_note))
                <div class="alert alert-info alert-dismissible fade in shadowed alert-dismissible mt-1 mb-1">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fa fa-info-circle"></i> Enabled SMS Alert  </strong>
                    <p>{{ trans('app.client_mobile_notice') }}</p>
                </div>
                @endif  
 
                @if($tokens->success || $tokens->stoped || $tokens->pending)
                <div class="alert alert-warning alert-dismissible fade in shadowed alert-dismissible mt-1 mb-1">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fa fa-info-circle"></i> Notice  </strong>
                    <p>{{ trans('app.client_token_generation_notice') }}</p>
                </div>   
                @endif    

                @if($tokens->success) 
                <div class="token-generator-msg alert alert-danger">
                    <p>{{ trans('app.no_success_token_found_in_last_10_tokens') }}</p> 
                </div>
                @elseif($tokens->stoped) 
                <div class="token-generator-msg alert alert-danger">
                    <p>{{ trans('app.you_have_been_blocked_for_the_day') }}</p> 
                </div>
                @elseif($tokens->pending)
                <div class="token-generator-msg alert alert-warning">
                    <p>{{ trans('app.wait_until_procced_the_pending_token') }}</p>
                </div>
                @endif 
 
                @if($tokens->display->show_note)
                    <!-- With Note -->
                    @foreach ($tokens->tokenButton as $data) 
                    <div class="p-1 m-1 btn btn-primary capitalize text-center">
                        <button 
                            type="button" 
                            class="p-1 m-1 btn btn-primary capitalize text-center"
                            style="min-width: 16vw;box-shadow:0px 0px 0px 2px#<?= substr(dechex(crc32($data->name)), 0, 6); ?>" 
                            data-toggle="modal" 
                            data-target="#tokenModal"
                            data-department-id="{{ $data->department_id }}"
                            data-counter-id="{{ $data->counter_id }}"
                            data-user-id="{{ $data->user_id }}"
                            >
                                <h5>{{ $data->name }}</h5>
                                <h6>{{ $data->officer }}</h6>
                        </button>  
                    </div>
                    @endforeach  
                    <!--Ends of With Note -->
                @else
                    <!-- Without Note -->
                    @foreach ($tokens->tokenButton as $data )
                      {{ Form::open(['url' => 'receptionist/token/auto', 'class' => 'AutoFrm p-1 m-1 btn btn-primary capitalize text-center']) }} 
                      <input type="hidden" name="department_id" value="{{ $data->department_id }}">
                      <input type="hidden" name="counter_id" value="{{ $data->counter_id }}">
                      <input type="hidden" name="user_id" value="{{ $data->user_id }}">
                      <button 
                        type="submit" 
                        class="p-1 m-1 btn btn-primary capitalize text-center"
                        style="min-width: 16vw;box-shadow:0px 0px 0px 2px#<?= substr(dechex(crc32($data->name)), 0, 6); ?>" 
                        >
                            <h5>{{ $data->name }}</h5>
                            <h6>{{ $data->officer }}</h6>
                    </button> 
                      {{ Form::close() }}
                    @endforeach 
                    <!--Ends of Without Mobile No -->
                @endif  
            </div>

            <div class="col-sm-6 about-us-text"> 
                <h1 class="text-left mb-1">{{ trans('app.todays_token') }} ({{ $tokens->total }})</h1>
                <table class="datatable display table table-bordered" width="100%" cellspacing="0">
                    <thead class="bg-info">
                        <tr>
                            <th>#</th>
                            <th>{{ trans('app.token_no') }}</th>
                            <th>{{ trans('app.details') }}</th> 
                            <th>{{ trans('app.created_at') }}</th>
                            <th width="120">{{ trans('app.action') }}</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @if (!empty($tokens->list))
                            <?php $sl = 1 ?>
                            @foreach ($tokens->list as $token)
                                <tr>
                                    <td>{{ $sl++ }}</td>
                                    <td>
                                        {!! (!empty($token->is_vip)?("<span class=\"label label-warning\" title=\"VIP\">$token->token_no</span>"):$token->token_no) !!} 
                                        <p class="p-0 m-0">
                                            @if($token->status==0) 
                                            <span class="label label-primary">{{ trans('app.pending') }}</span> 
                                            @elseif($token->status==1)   
                                            <span class="label label-success">{{ trans('app.complete') }}</span>
                                            @elseif($token->status==2) 
                                            <span class="label label-danger">{{ trans('app.stop') }}</span>
                                            @endif
                                            {!! (!empty($token->is_vip)?('<span class="label label-warning" title="VIP">VIP</span>'):'') !!}
                                        </p>
                                        {!! (!empty($token->position)?("<span class=\"label label-success\" title=\"Position\">$token->position</span>"):$token->position) !!} 
                                    </td>
                                    <td>
                                        <ul class="list-unstyled p-0 m-0">
                                            <li><strong>{{ trans('app.department') }}</strong> {{ !empty($token->department)?$token->department->name:null }}</li>
                                            <li><strong>{{ trans('app.counter') }}</strong> {{ !empty($token->counter)?$token->counter->name:null }}</li>
                                            <li><strong>{{ trans('app.officer') }}</strong> {!! (!empty($token->officer)?($token->officer->firstname." ". $token->officer->lastname):null) !!}</li>
                                        </ul> 
                                    </td> 
                                    <td>{{ (!empty($token->created_at)?date('j M Y h:i a',strtotime($token->created_at)):null) }}</td>
                                    <td>   
                                        <button type="button" href="{{ route('client.token.print') }}" data-token-id='{{ $token->id }}' class="tokenPrint btn btn-info btn-sm mb-1" title="Print" ><i class="fa fa-eye"></i> View</button>
                                        @if ($token->status == 0)
                                        <a href='{{ url("client/token/stoped/$token->id") }}'  class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')" title="Stoped"><i class="fa fa-stop"></i> Cancel</a>
                                        @endif 
                                    </td>
                                </tr> 
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>  
 

<div class="modal fade" tabindex="-1" id="tokenModal" role="dialog" style="z-index:100000">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      {{ Form::open(['url' => 'receptionist/token/auto', 'class' => 'AutoFrm']) }} 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ trans('app.note') }}</h4>
      </div>
      <div class="modal-body">  
        @if($tokens->display->show_note)
            <p>
                <textarea name="note" id="note" class="form-control" placeholder="{{ trans('app.note') }}">{{ old('note') }}</textarea>
                <span class="text-danger">The Note field is required!</span>
            </p>
        @endif 
        <input type="hidden" name="department_id">
        <input type="hidden" name="counter_id">
        <input type="hidden" name="user_id">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success hidden">{{ trans('app.submit') }}</button>
      </div>
      {{ Form::close() }}
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Preview Modal --> 
<div class="modal fade previewModal"  tabindex="-1" role="dialog" aria-labelledby="previewModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="previewModalLabel">{{ trans('app.token') }}</h4>
      </div>
      <div class="modal-body text-center"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
@endsection

@push("scripts")
<script type="text/javascript">
$(document).ready(function(){ 
    setInterval(function(){ 
        if($('.modal.in').length==0 && $('table>tbody>tr>td  .btn-danger').length>0){
            window.location.reload();
        }
    }, 60000);  

    $('#tokenModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('input[name=department_id]').val(button.data('department-id'));
        $('input[name=counter_id]').val(button.data('counter-id'));
        $('input[name=user_id]').val(button.data('user-id'));
        $("textarea[name=note]").val("");
        $('.modal button[type=submit]').addClass('hidden');
    });

    $('#tokenModal').on('hide.bs.modal', function () {
        $('.modal-backdrop').remove();
    });
 
    $("textarea[name=note]").on('keyup change', function(e){
        var valid = false;
        var noteErrorMessage = "";
        var note   = $('textarea[name=note]').val();

        if ($('textarea[name=note]').length)
        {
            if (note == '')
            {
                noteErrorMessage = "The Note field is required!";
                valid = false;
            }
            else if (note.length >= 255 || note.length <= 3)
            {
                noteErrorMessage = "The Note must be between 3-255 characters";
                valid = false;
            }
            else
            {
                noteErrorMessage = "";
                valid = true;
            }
        }
 
        if(!valid && noteErrorMessage.length > 0)
        {
            $('.modal button[type=submit]').addClass('hidden');
        } 
        else
        {
            $(this).next().html(" ");
            $('.modal button[type=submit]').removeClass('hidden');
        }
        $('textarea[name=note]').next().html(noteErrorMessage); 

    });

    var frm = $(".AutoFrm");
    frm.on('submit', function(e){
        e.preventDefault(); 
        $(".modal").modal('hide');
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: '{{ route("client.token") }}',
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            contentType: false,
            cache: false,
            processData: false,
            data:  formData,
            success: function(data)
            {
                if (data.status)
                {
                    var content = "<style type=\"text/css\">@media print {"+
                        "html, body {display:block;margin:0!important; padding:0 !important;overflow:hidden;display:table;}"+
                        ".receipt-token {width:100vw;height:100vw;text-align:center}"+
                        ".receipt-token h4{margin:0;padding:0;font-size:7vw;line-height:7vw;text-align:center}"+
                        ".receipt-token h1{margin:0;padding:0;font-size:15vw;line-height:20vw;text-align:center}"+
                        ".receipt-token ul{margin:0;padding:0;font-size:7vw;line-height:8vw;text-align:center;list-style:none;}"+
                        ".receipt-token img{width:500px}"+
                        "}</style>";

                    content += "<div class=\"receipt-token\">";
                    content += "<img src='{{ asset('public/assets/img/icons/logo.jpg') }}' alt='logo'/>";
                    content += "<h4>{{ \Session::get('app.title') }}</h4>";
                    content += "<h1>"+data.token.token_no+"</h1>";
                    content +="<ul class=\"list-unstyled\">";
                    content += "<li><strong>{{ trans('app.department') }} </strong>"+data.token.department+"</li>";
                    content += "<li><strong>{{ trans('app.counter') }} </strong>"+data.token.counter+"</li>";
                    content += "<li><strong>{{ trans('app.officer') }} </strong>"+data.token.firstname+' '+data.token.lastname+"</li>";
                    content += "<li><strong>{{ trans('app.date') }} </strong>"+data.token.created_at+"</li>";
                    content += "</ul>";
                    content += "</div>"; 
                    
                    // print 
                    // printThis(content);  
                    // setTimeout(function(){
                    //     history.go(0);
                    // }, 3000);  
                    $('.previewModal').modal('show').find('.modal-body').html(content);
                    $('.previewModal').on('hide.bs.modal', function (e) { 
                        history.go(0);
                    });


                    $("input[name=client_mobile]").val("");
                    $("textarea[name=note]").val("");
                    $('.modal button[type=submit]').addClass('hidden');
                }
            },
            error: function(xhr)
            {
                alert('wait...');
            }
        });
    }); 

    // print token
    $("body").on("click", ".tokenPrint", function(e) { 
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type:'POST',
            dataType: 'json',
            data: {
                'id' : $(this).attr('data-token-id'),
                '_token':'<?php echo csrf_token() ?>'
            },
            success:function(data)
            {
                var content = "<style type=\"text/css\">@media print {"+
                       "html, body {display:block;margin:0!important; padding:0 !important;overflow:hidden;display:table;}"+
                       ".receipt-token {width:100vw;height:100vw;text-align:center}"+
                       ".receipt-token h4{margin:0;padding:0;font-size:7vw;line-height:7vw;text-align:center}"+
                       ".receipt-token h1{margin:0;padding:0;font-size:15vw;line-height:20vw;text-align:center}"+
                       ".receipt-token ul{margin:0;padding:0;font-size:7vw;line-height:8vw;text-align:center;list-style:none;}"+
                       "}</style>";
                content += "<div class=\"receipt-token\">";
                content += "<h4>{{ \Session::get('app.title') }}</h4>";
                content += "<h1>"+data.token_no+"</h1>";
                content +="<ul class=\"list-unstyled\">";
                content += "<li><strong>{{ trans('app.department') }} </strong>"+data.department+"</li>";
                content += "<li><strong>{{ trans('app.counter') }} </strong>"+data.counter+"</li>";
                content += "<li><strong>{{ trans('app.officer') }} </strong>"+data.firstname+' '+data.lastname+"</li>";
                if (data.note)
                {
                    content += "<li><strong>{{ trans('app.note') }} </strong>"+data.note+"</li>";
                }
                content += "<li><strong>{{ trans('app.date') }} </strong>"+data.created_at+"</li>";
                content += "</ul>";  
                content += "</div>";    
     
                // print 
                // printThis(content);
                $('.previewModal').modal('show').find('.modal-body').html(content);

            }, error:function(err){
                alert('failed!');
            }
        });  
    });
});
</script>
@endpush
 
 