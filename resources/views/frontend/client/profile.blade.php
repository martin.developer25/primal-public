@extends('layouts.frontend')
 
@section('content')
<!-- Page --> 
<section id="dashboard" class="common-section">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12">
                <div class="about-us-text">
                    <h5 class="text-uppercase">{{ trans('app.profile_setting') }}</h5> 

                    @include('backend.common.info')
                    @yield('info.message')  

                    {{ Form::open(['route'=>'client.profile', 'method'=>'put', 'files'=>true, 'id'=>'profileFrm', 'class'=>'row']) }}

                        @if(empty($user->mobile) && !empty($display) && ($display->sms_alert=='1'))
                        <input name="mobile_required" type="hidden" value="1">
                        @endif

                        <div class="col-sm-4 col-md-3">
                            <img src="{{ asset((session('photo')?session('photo'):(!empty($user->photo)?$user->photo:'public/assets/img/icons/no_user.jpg'))) }}" alt="Photo" class="thumbnail">

                            <input type="hidden" name="old_photo" value="{{ ((session('photo') != null) ? session('photo') : $user->photo) }}">  

                            <div class="form-group @error('photo') has-error @enderror">
                                <input name="photo" type="file" placeholder="{{ trans('app.photo') }}" class="form-control" id="photo" value="{{ old('photo')?old('photo'):$user->photo }}">
                                <label for="photo">{{ trans('app.photo') }}</label>
                                <p class="help-block">{{ $errors->first('photo') }}</p>
                            </div>  
                        </div> 

                        <div class="col-sm-8 col-md-9">  
                            <div class="row clear">
                                <div class="col-sm-6">
                                    <div class="form-group @error('firstname') has-error @enderror">
                                        <input name="firstname" type="text" placeholder="{{ trans('app.firstname') }}" class="form-control" id="firstname" value="{{ old('firstname')?old('firstname'):$user->firstname }}">
                                        <label for="firstname">{{ trans('app.firstname') }}</label>
                                        <p class="help-block">{{ $errors->first('firstname') }}</p>
                                    </div> 
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group @error('lastname') has-error @enderror">
                                        <input name="lastname" type="text" placeholder="{{ trans('app.lastname') }}" class="form-control" id="lastname" value="{{ old('lastname')?old('lastname'):$user->lastname }}">
                                        <label for="lastname">{{ trans('app.lastname') }}</label>
                                        <p class="help-block">{{ $errors->first('lastname') }}</p>
                                    </div> 
                                </div> 
                            </div> 

                            <div class="form-group @error('email') has-error @enderror">
                                <input name="email" type="text" placeholder="{{ trans('app.email') }}" class="form-control" id="email" value="{{ old('email')?old('email'):$user->email }}">
                                <label for="email">{{ trans('app.email') }}</label>
                                <p class="help-block">{{ $errors->first('email') }}</p>
                            </div>

                            <div class="row clear">
                                <div class="col-sm-6">
                                    <div class="form-group @error('password') has-error @enderror">
                                        <input name="password" type="password" placeholder="{{ trans('app.password') }}" class="form-control" id="password">
                                        <label for="password">{{ trans('app.password') }}</label>
                                        <p class="help-block">{{ $errors->first('password') }}</p>
                                    </div> 
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group @error('conf_password') has-error @enderror">
                                        <input name="conf_password" type="password" placeholder="{{ trans('app.conf_password') }}" class="form-control" id="conf_password">
                                        <label for="conf_password">{{ trans('app.conf_password') }}</label>
                                        <p class="help-block">{{ $errors->first('conf_password') }}</p>
                                    </div>   
                                </div> 
                            </div> 
 
                            <div class="form-group @error('mobile') has-error @enderror {{ ((empty($user->mobile) && !empty($display) && ($display->sms_alert=='1'))?'has-error':null) }}">
                                <input name="mobile" type="text" placeholder="{{ trans('app.mobile') }}" class="form-control" id="mobile" value="{{ old('mobile')?old('mobile'):$user->mobile }}">
                                <label for="mobile">{{ trans('app.mobile') }}</label>
                                <p class="help-block">{{ $errors->first('mobile') }}</p>
                            </div>

                            <div class="form-group @error('about') has-error @enderror">
                                <textarea name="about" type="text" placeholder="{{ trans('app.about') }}" class="form-control" id="about">{{ old('about')?old('about'):$user->about }}</textarea>
                                <label for="about">{{ trans('app.about') }}</label>
                                <p class="help-block">{{ $errors->first('about') }}</p>
                            </div> 
                            
                            <div class="row clear">
                                <div class="col-sm-6">   
                                </div>
                                <div class="col-sm-6 text-right">  
                                    <div class="btn-group">
                                        <button type="reset" class="btn btn-info">{{ trans('app.reset') }}</button>
                                        <button type="submit" class="btn thm-btn form-btn">{{ trans('app.update') }}</button>
                                    </div>
                                </div>
                            </div>     
                        </div>
                    {{ Form::close() }} 
                </div>
            </div>
        </div>
    </div>
</section>  
@endsection 
