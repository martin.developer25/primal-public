<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ \Session::get('app.title') }} :: @yield('title')</title>
    <!-- favicon -->
    <link href="{{ asset((!empty(\Session::get('app.favicon'))?\Session::get('app.favicon'):'public/assets/img/icons/favicon.ico')) }}" rel="icon" type="image/x-icon"/>
    <!-- template bootstrap -->
    <link href="{{ asset('public/assets/css/template.min.css') }}" rel='stylesheet prefetch'>
    <!-- roboto -->
    <link href="{{ asset('public/assets/css/roboto.css') }}" rel='stylesheet'>
    <!-- material-design -->
    <link href="{{ asset('public/assets/css/material-design.css') }}" rel='stylesheet'>
    <!-- small-n-flat -->
    <link href="{{ asset('public/assets/css/small-n-flat.css') }}" rel='stylesheet'>
    <!-- font-awesome -->
    <link href="{{ asset('public/assets/css/font-awesome.min.css') }}" rel='stylesheet'>
    <!-- jquery-ui -->
    <link href="{{ asset('public/assets/css/jquery-ui.min.css') }}" rel='stylesheet'>
    <!-- datatable -->
    <link href="{{ asset('public/assets/css/dataTables.min.css') }}" rel='stylesheet'>
    <!-- select2 -->
    {{-- <link href="{{ asset('public/assets/css/select2.min.css') }}"  rel='stylesheet'> --}}
    <!-- custom style -->
    <link href="{{ asset('public/assets/css/style.css') }}" rel='stylesheet'>
    <!-- Page styles --> 
    <link href="{{ asset('public/assets/css/departament-token.css') }}" rel='stylesheet'>
    @stack('styles')

    <!-- Jquery  -->
    <script src="{{ asset('public/assets/js/jquery.min.js') }}"></script>
</head>
<body class="cm-no-transition cm-1-navbar loader-process">
    @include('backend.common.info')

    <div class="loader">
        <div>
            <span>P</span>
            <span>L</span>
            <span>A</span>
            <span>N</span>
            <span>S</span>
            <span>U</span>
            <span>A</span>
            <span>R</span>
            <span>E</span>
            <span>Z</span>
        </div>
    </div>
    <div class="div-principal">
        <div class="div-principal">
            <!-- Starts of Message -->
            @yield('info.message')
            <!-- Ends of Message --> 

            <!-- Starts of Content -->
            @yield('content')
            <!-- Ends of Contents --> 
        </div>

        <!-- Starts of Copyright -->
            
        <footer class="cm-footer text-right" style="display: none;">
            <span class="hidden-xs">{{ \Session::get('app.copyright_text') }}</span>
            <span class="pull-left text-center">@yield('info.powered-by') @yield('info.version')</span> 
        </footer>
        <!-- Ends of Copyright -->
    </div>


    <!-- All js -->
    <!-- bootstrp -->
    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
    <!-- juery-ui -->
    <script src="{{ asset('public/assets/js/jquery-ui.min.js') }}"></script> 
    <!-- jquery.mousewheel.min -->
    <script src="{{ asset('public/assets/js/jquery.mousewheel.min.js') }}"></script>
    <!-- jquery.cookie.min -->
    <script src="{{ asset('public/assets/js/jquery.cookie.min.js') }}"></script>
    <!-- fastclick -->
    <script src="{{ asset('public/assets/js/fastclick.min.js') }}"></script>
    <!-- template -->
    <script src="{{ asset('public/assets/js/template.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('public/assets/js/dataTables.min.js') }}"></script>
    <!-- custom script -->
    <script src="{{ asset('public/assets/js/script.js') }}"></script>
    
    <!-- Page Script -->
    @stack('scripts')
    
    <script type="text/javascript">
    (function() {
      //notification
        notify();
        setInterval(function(){
            notify();
        }, 30000);

        function notify()
        {
            $.ajax({
               type:'GET',
               url:'{{ URL::to("common/message/notify") }}',
               data:'_token = <?php echo csrf_token() ?>',
               success:function(data){
                  $("#message-notify").html(data);
               }
            });
        }
     
        //language switch
        $(".select-lang").on('click', function() { 
            $.ajax({
               type:'GET',
               url: '{{ url("common/language") }}',
               data: {
                  'locale'   : $(this).data("locale"), 
                  '_token'   : '<?php echo csrf_token() ?>'
               },
               success:function(data){
                  history.go(0);
               }, error: function() {
                alert('failed');
               }
            });       
        });
        
    })();
    </script>
</body>
</html>

 