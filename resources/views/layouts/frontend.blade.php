<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url" content="{{ url('') }}">
    <meta name="description" content="{{ (!empty($app->meta_description)?$app->meta_description:null) }}">
    <meta name="keywords" content="{{ (!empty($app->meta_keyword)?$app->meta_keyword:null) }}">
    <title>{{ (!empty($app->meta_title)?$app->meta_title:(!empty($app->title)?$app->title:'CodeKernel')) }}</title>
    <!-- Favicon-->
    <link href="{{ asset((!empty($app->favicon)?$app->favicon:'public/assets/img/icons/favicon.ico')) }}" rel="icon" type="image/x-icon"/>
    <!-- Bootstrap -->
    <link href="{{ asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/> 
    <!-- Page styles --> 
    @stack('styles')
    <!-- style -->
    <link href="{{ asset('public/assets/css/website.css') }}" rel="stylesheet" type="text/css"/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('public/assets/js/jquery.min.js') }}"></script>
</head>
<body id="page-top"> 
    <nav class="navbar navbar-inverse navbar-xs" id="home"> 
        <div class="container text-right">
         <ul class="nav navbar-nav navbar-right">   
            @if(auth()->check() && auth()->user()->isClient())  
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                    <i class="fa fa-dashboard"></i>
                    {{ trans('app.dashboard') }} 
                    <!-- <span class="bell">3</span>  -->
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu"> 
                    <li>
                        <a href="{{ route('client.dashboard') }}">
                            <i class="fa fa-tachometer"></i>
                            {{ trans('app.dashboard') }} 
                        </a>
                    </li> 
                    <li>
                        <a href="{{ route('client.token') }}">
                            <i class="fa fa-ticket"></i> 
                            {{ trans('app.token') }} 
                        </a>
                    </li>  
                    <li>
                        <a href="{{ route('client.report') }}">
                            <i class="fa fa-pie-chart"></i> 
                            {{ trans('app.reports') }} 
                        </a>
                    </li>  
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    {{ trans('app.profile') }}
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu"> 
                    <li><a href="{{ route('client.profile') }}"><i class="fa fa-cog"></i>&nbsp;{{ trans('app.profile_setting') }}</a></li> 
                    <li><a href="{{ route('client.signout') }}"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{ trans('app.signout') }}</a></li>
                </ul>
            </li>
            @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-unlock-alt"></i>
                    {{ trans('app.signin') }}
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a style="width:45%" class="page-scroll" href="#get-started"><i class="fa fa-user"></i> {{ trans('app.client') }}</a>
                        <a style="width:45%" href="{{ route('auth.login') }}" target="_blank"><i class="fa fa-user-secret"></i> {{ trans('app.admin') }}</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
        </div> 
    </nav>
 
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" id="toggleX" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset((!empty($app->logo)?$app->logo:'public/assets/img/icons/logo.png')) }}" alt="" height="60px" />
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="nav navbar-nav navbar-right">
                    @if (session()->has('menus'))
                    @foreach(session()->get('menus') as $url => $menu)
                        <li class="{{ $loop->index==0?'active':'' }}">
                            <a class="{{ (strripos($url, '#')!==false)?'page-scroll':'' }}" href="{{ $url }}">{{ $menu }}</a>
                        </li>
                    @endforeach 
                    @endif 
                  </ul>
                </ul>
            </div> 
        </div> 
    </nav>

    @yield('content')

    <!-- footer -->
    <footer class="navbar-inverse">
        <div class="container-fluid">
            <div class="row">
                @include('backend.common.info')
                <div class="col-sm-4 text-left footer-text">
                    @yield('info.powered-by') 
                </div>
                <div class="col-sm-4 text-center">
                    <div class="footer-text">
                        {{ (!empty($app->copyright_text)?$app->copyright_text:'CodeKernel.Net') }}
                    </div>
                </div>
                <div class="col-sm-4 text-right">
                    <div class="footer-text">@yield('info.version')</div>
                </div>
            </div>
        </div> 
    </footer>  

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/jquery.easing.min.js') }}"></script>

    <script type="text/javascript">
    $(document).ready(function () {  
        var menuTop = $('.navbar-default').offset().top; 
        $(window).scroll(function () {
            if ($(window).scrollTop() > menuTop) {
                $('.navbar-default').css({
                    position: 'fixed',
                    top: '0px'
                });
            } else {
                $('.navbar-default').css({
                    position: 'static',
                    top: '0px'
                });
            }
        }); 
  
        // jQuery for page scrolling feature - requires jQuery Easing plugin
        $('a.page-scroll').bind('click', function (event) {
            event.preventDefault();
            var anchor =  $(this).attr('href');
            var url    =  window.location.href;
            var page   =  url.search('/page') !== -1 || url.search('/client') !== -1;
            if (page) {
                window.location.href = $('meta[name=url]').attr('content')+'/'+anchor;
            } else {
                $('html, body').stop().animate({
                    scrollTop: ($(anchor).offset().top - 50)
                }, 1250, 'easeInOutExpo');
            }
        });

        // Highlight the top nav as scrolling occurs
        $('body').scrollspy({
            target: '.navbar-fixed',
            offset: 100
        });

        // Closes the Responsive Menu on Menu Item Click
        $('.navbar-collapse ul li a').click(function () {
            $('.navbar-toggle:visible').click();
        });

        // back to top
        $('body').append('<div id="toTop" class="btn back-top"><span class="fa fa-arrow-up"></span></div>');
        $(window).on("scroll", function () {
            if ($(this).scrollTop() !== 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });
        $('#toTop').on("click", function () {
            $("html, body").animate({scrollTop: 0}, 1250);
            return false;
        });

        // Closes the Responsive Menu on Menu Item Click
        $('.navbar-collapse ul li a').click(function () {
            $('.navbar-toggle:visible').click();
        });  
    });

     //print a div
    function printThis(content = "", reload = false) {

        if (content.length < 64 && $('#' + content).length > 0) { 
            // if element length less than 64 characters and is a ID
            content = $('head').html() + $('#' + content).clone().html();
        }  

        try {
            var ua = navigator.userAgent;

            if (/Chrome/i.test(ua)) {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                })
                .appendTo('body')
                .contents().find('body')
                .append(content);

                setTimeout(() => { 
                    window.frames['myiframe'].focus();
                    window.frames['myiframe'].print();
                    $('iframe.printFrame').remove();
                }, 200);

            } else if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)) {     
               
                var win = window.open('about:blank', 'Token' + (new Date()).getTime());
                win.document.write(content); 

                setTimeout(function () {
                    win.document.close();
                    win.focus();
                    win.print();
                    win.close(); 
                }, 200);   

            } else {

                var originalContent = $('body').html();
                $('body').empty().html(content);
                window.print();
                $('body').html(originalContent);

            }

        } catch(e) {

            var originalContent = $('body').html();
            $('body').empty().html(content);
            window.print();
            $('body').html(originalContent);

        }

        if (reload) {
            history.go(0);
        }
    }
    </script>
    
    <!-- Page Script -->
    @stack('scripts')
</body>
</html>